//
//  AppDelegate.m
//  GNETS
//
//  Created by tcnj on 16/2/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//


#import "AppDelegate.h"
#import "IQKeyboardManager.h"
#import "ViewController.h"
#import "JPUSHService.h"
#import "CMManager.h"
#import "LoginViewController.h"

//地图
#import "APIKey.h"
#import <AMapNaviKit/AMapNaviKit.h>
#import <AMapNaviKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import "AMapLocationKit.h"
#import "iflyMSC/IFlySpeechSynthesizer.h"
#import "iflyMSC/IFlySpeechSynthesizerDelegate.h"
#import "iflyMSC/IFlySpeechConstant.h"
#import "iflyMSC/IFlySpeechUtility.h"
#import "iflyMSC/IFlySetting.h"
#import "AFNetworkActivityIndicatorManager.h"

#import <Bugly/CrashReporter.h>
@interface AppDelegate ()
@end
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
   //self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    //键盘配置
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [IQKeyboardManager sharedManager].shouldShowTextFieldPlaceholder = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    //极光推送
    [self initJPushMethod:launchOptions];
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;

    //配置地图
    [self configureAPIKey];
    [self configIFlySpeech];
    
    //创建数据库
    [GNETSSqlitManager copyOrCreateDB];
    

    
    [NSThread sleepForTimeInterval:3.f];
    
    //Buglt初始化
    [[CrashReporter sharedInstance] enableLog:YES];
    [[CrashReporter sharedInstance] installWithAppId:@"900026235"];
//    [self performSelector:@selector(crash) withObject:nil afterDelay:3.0];
    
    ViewController *viewVC = [ViewController new];
    self.window.rootViewController = viewVC;
    [self.window makeKeyAndVisible];
    return YES;
}
- (void)setupBugly {

}
- (void)configureAPIKey
{
    if ([APIKey length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"apiKey为空，请检查key是否正确设置" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    
    [AMapNaviServices sharedServices].apiKey = (NSString *)APIKey;
    [MAMapServices sharedServices].apiKey = (NSString *)APIKey;
    [AMapSearchServices sharedServices].apiKey = (NSString *)APIKey;
    [AMapLocationServices sharedServices].apiKey =(NSString *)APIKey;
}
- (void)configIFlySpeech
{
    [IFlySpeechUtility createUtility:[NSString stringWithFormat:@"appid=%@,timeout=%@",@"5565399b",@"20000"]];
    
    [IFlySetting setLogFile:LVL_NONE];
    [IFlySetting showLogcat:NO];
    
    // 设置语音合成的参数
    [[IFlySpeechSynthesizer sharedInstance] setParameter:@"50" forKey:[IFlySpeechConstant SPEED]];//合成的语速,取值范围 0~100
    [[IFlySpeechSynthesizer sharedInstance] setParameter:@"50" forKey:[IFlySpeechConstant VOLUME]];//合成的音量;取值范围 0~100
    
    // 发音人,默认为”xiaoyan”;可以设置的参数列表可参考个 性化发音人列表;
    [[IFlySpeechSynthesizer sharedInstance] setParameter:@"xiaoyan" forKey:[IFlySpeechConstant VOICE_NAME]];
    
    // 音频采样率,目前支持的采样率有 16000 和 8000;
    [[IFlySpeechSynthesizer sharedInstance] setParameter:@"8000" forKey:[IFlySpeechConstant SAMPLE_RATE]];
    
    // 当你再不需要保存音频时，请在必要的地方加上这行。
    [[IFlySpeechSynthesizer sharedInstance] setParameter:nil forKey:[IFlySpeechConstant TTS_AUDIO_PATH]];
}

#pragma mark -极光推送
- (void)initJPushMethod:(NSDictionary *)launchOptions
{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(networkDidReceiveMessage:) name:kJPFNetworkDidReceiveMessageNotification object:nil];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //可以添加自定义categories
        [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                          UIUserNotificationTypeSound |
                                                          UIUserNotificationTypeAlert)
                                              categories:nil];
    } else {
        //categories 必须为nil
        [JPUSHService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
                                                          UIRemoteNotificationTypeSound |
                                                          UIRemoteNotificationTypeAlert)
                                              categories:nil];
    }
    
    [JPUSHService setupWithOption:launchOptions appKey:appKey
                          channel:channel apsForProduction:isProduction];
}
- (void)networkDidReceiveMessage:(NSNotification *)notification
{
    NSDictionary * userInfo = [notification userInfo];
    
    NSString *content = [userInfo valueForKey:@"content"];
    NSDictionary *extras = [userInfo valueForKey:@"extras"];
    NSString *eventType = [NSString stringWithFormat:@"%@",[extras valueForKey:@"eventType"]]; //自定义参数，key是自己定义的
    //8关闭超时 7关闭成功 6开启超时 5开启成功
    
    NSString *userName = [USER_D objectForKey:@"user_phone"];

    if (userName.length >0)
    {
        if ([eventType isEqualToString:@"10"])
        {
            [USER_D removeObjectForKey:@"user_phone"];
            [USER_D removeObjectForKey:@"user_password"];
            [USER_D synchronize];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"下线通知" message:content delegate:self cancelButtonTitle:nil otherButtonTitles:@"确认", nil];
            //alertView.alertViewStyle = UIAlertViewStyleDefault;
            [alertView show];
        }else{
            [JKPromptView showWithImageName:nil message:content];
        }
        
        if ([eventType isEqualToString:@"9"])
        {
            [[SoundManager sharedSoundManager] musicPlayByName:@"msg_prompt"];
        }
        if (eventType.intValue == 8 || eventType.intValue == 7 || eventType.intValue == 6 || eventType.intValue == 5) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"OpenVf" object:nil userInfo:@{@"eventType":eventType}];
        }

    }
    
//    LogRed(@"自定义消息1   %@",content);
//    LogRed(@"自定义消息2   %@",extras);
//    LogRed(@"自定义消息3   %@",customizeField1);
//    
//    XYShowAlert(content, @"我知道了");
    DLog(@"消息是－－－－－%@",userInfo);
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex ==0)
    {
        [USER_D removeObjectForKey:@"WARNMSG"];
        [USER_D removeObjectForKey:@"UNREAD"];
        [USER_D synchronize];
        LoginViewController *login = [[LoginViewController alloc] init];
        UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:login];
        nav.navigationBarHidden = YES;
        UIWindow *window =[[[UIApplication sharedApplication] windows] objectAtIndex:0];
        window.rootViewController = nav;
    }
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    DLog(@"token---:%@",[[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""]stringByReplacingOccurrencesOfString: @">" withString: @""]stringByReplacingOccurrencesOfString: @" " withString: @""]);
    [JPUSHService registerDeviceToken:deviceToken];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    //点击进来时候
    // IOS 7 Support Required
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    //Optional
    DLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
//判断不同系统下用户是否在设置界面关闭了推送
- (BOOL)isAllowedNotification {
    
    //iOS8 check if user allow notification
    if ([[CMManager sharedCMManager] isSystemVersioniOS8])
    {
        // system is iOS8
        UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
        if (UIUserNotificationTypeNone != setting.types)
        {
            return YES;
        }
    }
    else
    {
        //iOS7
        UIRemoteNotificationType type = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if(UIRemoteNotificationTypeNone != type)
            return YES;
    }
    
    return NO;
}


//程序成为活动状态后走的方法
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    BOOL isOpenNotify = [self isAllowedNotification];
    //开启推送
    if (isOpenNotify)
    {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    //关闭推送
    else
    {
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
    
    //    if (ISIos8)
    //    {
    //        BOOL isRemoteNotify = [UIApplication sharedApplication].isRegisteredForRemoteNotifications;
    //        if (isRemoteNotify)
    //        {
    //            [[UIApplication sharedApplication] registerForRemoteNotifications];
    //        }
    //        else
    //        {
    //            [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    //        }
    //    }
    //    else
    //    {
    //        //用户关闭了推送
    //        if ([[UIApplication sharedApplication] enabledRemoteNotificationTypes] == UIRemoteNotificationTypeNone)
    //        {
    //            [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    //        }
    //        else
    //        {
    //            [[UIApplication sharedApplication] registerForRemoteNotifications];
    //        }
    //    }
    
    
}


@end
