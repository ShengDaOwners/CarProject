//
//  GNETSSqlitManager.m
//  GNETS
//
//  Created by fyc on 16/3/1.
//  Copyright © 2016年 CQZ. All rights reserved.
//
#import "FMDatabase.h"
#import "GNETSSqlitManager.h"
#import "Country.h"
#import "Province.h"
#import "City.h"
#import "Districtlist.h"

@implementation GNETSSqlitManager

//根全局变量一样
static FMDatabase *db ;

#pragma -mark 创建数据库
+ (void)copyOrCreateDB{
    
    //    NSArray *searchPaths = NSSearchPathForDirectoriesInDomains(
    //                                                               NSDocumentDirectory,
    //                                                               NSUserDomainMask,
    //                                                               YES);
    //    NSString *documentFolderPath = [searchPaths objectAtIndex:0];
    //    NSLog(@"docoumentFolderPath=%@",documentFolderPath);
    //
    //    NSString *dbFilePath = [documentFolderPath stringByAppendingPathComponent:@"ebike_city.db"];
    //
    //    NSString * dbFilePath = [NSHomeDirectory() stringByAppendingPathComponent:@"ebike_city.db"];
    //
    //    NSLog(@"dbFilePath = %@",dbFilePath);
    //
    //    NSFileManager *fm = [NSFileManager defaultManager];
    //
    //    //2. 通过 NSFileManager 对象 fm 来判断文件是否存在，存在 返回YES  不存在返回NO
    //    BOOL isExist = [fm fileExistsAtPath:dbFilePath];
    //
    //    //如果不存在 isExist = NO，拷贝工程里的数据库到Documents下
    //    if (!isExist)
    //    {
    //        //拷贝数据库
    //
    //        //获取工程里，数据库的路径,因为我们已在工程中添加了数据库文件，所以我们要从工程里获取路径
    //        NSString *backupDbPath = [[NSBundle mainBundle]
    //                                  pathForResource:@"ebike_city"
    //                                  ofType:@"db"];
    //        //这一步实现数据库的添加，
    //        // 通过NSFileManager 对象的复制属性，把工程中数据库的路径拼接到应用程序的路径上
    //        BOOL cp = [fm copyItemAtPath:backupDbPath toPath:dbFilePath error:nil];
    //        NSLog(@"cp = %d",cp);
    //
    //    }
    //    NSLog(@"isExist =%d",isExist);
    //
    //    db  = [[FMDatabase alloc]initWithPath:dbFilePath];
    //
    //    //shujuku dakai
    //    [db open];
    //
    //    NSLog( @"数据库 open " );
    //
    //    [db setShouldCacheStatements:YES];
    
    //获得database.sqlite在束的路径
    NSString *shuPath = [[NSBundle mainBundle] pathForResource:@"ebike_city" ofType:@"db"];
    
    NSString *homePath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/database.sqlite"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ( ![fileManager fileExistsAtPath:homePath] ) {
        
        [fileManager copyItemAtPath:shuPath toPath:homePath error:nil];
        
    }
    
    db = [[FMDatabase alloc] initWithPath:homePath];
    
    [db open];
    
    DLog( @"数据库 open " );
    
    [db setShouldCacheStatements:YES];
}
/*
 *获取国家列表
 */
+ (NSMutableArray *)getCountryList
{
    //打开数据库
    if (![db open])
    {
        NSLog(@"数据库打开失败");
    }
    [db setShouldCacheStatements:YES];
    
    //查询 结果集
    FMResultSet * rs = [db executeQuery:@"select * from countryList"];
    
    NSMutableArray *ar  = [[NSMutableArray alloc]init];
    
    while ([rs next])
    {
        Country * country = [[Country alloc]init];
        
        country._id = [rs intForColumn:@"_id"];
        
        country.cid = [rs intForColumn:@"cid"];
        
        country.name = [rs stringForColumn:@"name"];
        
        NSLog(@"heh===%@",country.name);
        [ar addObject:country];
        
    }
    
    return ar;
}
/*
 *获取省列表
 */
+ (NSMutableArray *)getProvinceListWithContryId:(NSString *)contryId{
    
    //打开数据库
    if (![db open])
    {
        NSLog(@"数据库打开失败");
    }
    [db setShouldCacheStatements:YES];
    
    //查询 结果集
    
    FMResultSet * rs = [db executeQuery:@"select * from provinceList where country_id = ?",contryId];
    
    NSMutableArray *ar  = [[NSMutableArray alloc]init];
    
    while ([rs next])
    {
        Province * province = [[Province alloc]init];
        
        province._id = [rs intForColumn:@"_id"];
        
        province.pid = [rs intForColumn:@"pid"];
        
        province.name = [rs stringForColumn:@"name"];
        
        province.country_id = [rs intForColumn:@"country_id"];

        [ar addObject:province];
        
    }
    
    return ar;
    
}
/*
 *获取市列表
 */
+ (NSMutableArray *)getCityeListWithProvinceId:(NSString *)ProvinceId{
    
    //打开数据库
    if (![db open])
    {
        NSLog(@"数据库打开失败");
    }
    [db setShouldCacheStatements:YES];
    
    //查询 结果集
    
    FMResultSet * rs = [db executeQuery:@"select * from cityList where province_id = ?",ProvinceId];
    
    NSMutableArray *ar  = [[NSMutableArray alloc]init];
    
    while ([rs next])
    {
        City * city = [[City alloc]init];
        
        city._id = [rs intForColumn:@"_id"];
        
        city.cid = [rs intForColumn:@"cid"];
        
        city.name = [rs stringForColumn:@"name"];
        
        city.province_id = [rs intForColumn:@"province_id"];
        
        [ar addObject:city];
        
    }
    
    return ar;
    
}
/*
 *获取地区列表
 */
+ (NSMutableArray *)getDistrictListWithCityId:(NSString *)CityId{
    
    //打开数据库
    if (![db open])
    {
        NSLog(@"数据库打开失败");
    }
    [db setShouldCacheStatements:YES];
    
    //查询 结果集
    
    FMResultSet * rs = [db executeQuery:@"select * from districtList where city_id = ?",CityId];
    
    NSMutableArray *ar  = [[NSMutableArray alloc]init];
    
    while ([rs next])
    {
        Districtlist * district = [[Districtlist alloc]init];
        
        district._id = [rs intForColumn:@"_id"];
        
        district.did = [rs intForColumn:@"did"];
        
        district.name = [rs stringForColumn:@"name"];
        
        district.city_id = [rs intForColumn:@"city_id"];
        
        [ar addObject:district];
        
    }
    
    return ar;
    
}
@end
