//
//  StatisticalVC.m
//  GNETS
//
//  Created by tcnj on 16/2/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "StatisticalVC.h"
#import "StatisticKMCell.h"
#import "StatisticModel.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerController.h"

@interface StatisticalVC ()<UICollectionViewDelegate,UICollectionViewDataSource,PNChartDelegate>

@property (nonatomic, retain)UICollectionView *collectionView;
@property (nonatomic, retain)PNBarChart * barChart;
@property (nonatomic, retain)PNLineChart *lineChart;
@property (nonatomic, retain)NSMutableArray *headData;
@property (nonatomic, retain)NSArray *itemData;
@property (nonatomic, retain)StatisticModel *statisticModel;
@property (nonatomic, retain)UIScrollView *baseScroll;
@property (nonatomic, assign)NSInteger nowRow;//标示 点击哪一个item
@property (nonatomic, assign)NSInteger chartHeight;

@end

static NSString * const kCellIdentifier           = @"CellIdentifier";
static NSString * const kHeaderViewCellIdentifier = @"HeaderViewCellIdentifier";


@implementation StatisticalVC
- (NSMutableArray *)headData{
    if (!_headData) {
        _headData = [[NSMutableArray alloc]initWithCapacity:0];
    }
    return _headData;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNaviBarWithTitle:@"行车数据统计"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:nil img:@"icon_title_user"];
    
    //UI
    [self addCollectionView];
    //数据
    [self getTodayReuqest];
    
}
- (void)addCollectionView {
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    layout.minimumInteritemSpacing = 0;//item 之间左右间距
    layout.minimumLineSpacing = 0;//item之间上下间距
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 64, kMainScreenWidth,kMainScreenHeight - 64 - 49) collectionViewLayout:layout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerNib:[UINib nibWithNibName:@"StatisticKMCell" bundle:nil] forCellWithReuseIdentifier:kCellIdentifier];
    self.collectionView.allowsMultipleSelection = YES;
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kHeaderViewCellIdentifier];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.scrollsToTop = NO;
    [self.view addSubview:self.collectionView];
    
    self.chartHeight = kMainScreenHeight - 64 - 49 - 160  > 0?kMainScreenHeight - 64 - 49 - 160:300;
    self.baseScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0
                                                                     , kMainScreenWidth, self.chartHeight)];
    self.baseScroll.contentSize = CGSizeMake(kMainScreenWidth*2, self.chartHeight);
    //    self.baseScroll.pagingEnabled = YES;
    self.baseScroll.contentOffset = CGPointMake(kMainScreenWidth, 0);
    self.baseScroll.bounces = NO;
    self.baseScroll.delegate = self;
    self.baseScroll.backgroundColor = [UIColor colorWithHexString:navColor];
    self.baseScroll.showsHorizontalScrollIndicator = NO;
    self.baseScroll.showsVerticalScrollIndicator = NO;
    
    self.nowRow = -1;//区别于indexPath
    
    self.barChart = [self getBarChart];
    //    [self.baseScroll addSubview:self.barChart];
    
    self.lineChart = [self getLineChart];
    //    [self.baseScroll addSubview:self.lineChart];
    
    
}
#pragma mark -获取数据
- (void)getSectionDateRequest{
    
    UserModel *user = [[PublicFunction ShareInstance]getAccount];
    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@&dayNum=%@",kProjectBaseUrl,StaticSectionDateInfo,user.data.carId,@"15"] isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        [PublicFunction showErrorMsg:jsonDic];
        
        NSArray *dataArr = jsonDic[@"data"];
        NSMutableArray *dateArr = [[NSMutableArray alloc]initWithCapacity:0];
        for (NSDictionary *d in dataArr) {
            StatisticModel *model = [[StatisticModel alloc]initWithArrAndDic:d];
            [self.headData addObject:model];
            [dateArr addObject:model.date];
        }
        //检查今天（对比时间）是否调用过15天的数据，如果没有就调用，反之不调用
        if (![dataArr containsObject:_statisticModel.date]) {
            [self.headData addObject:_statisticModel];
        }
        //根据时间进行排序
        [self sortTimeWithArr:self.headData];
        
        [self reloadPnchartWithIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]
        ;
        
        
    } fail:^{
        
    }];
    
}
- (void)getTodayReuqest{
    
    NSDate *  senddate=[NSDate date];
    
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    
    //    [dateformatter setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"zh_CN"]];//location设置为中国
    
    [dateformatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    
    NSLog(@"locationString:%@",locationString);
    
    UserModel *user = [[PublicFunction ShareInstance]getAccount];
    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@&date=%@",kProjectBaseUrl,StaticSofEveryDay,user.data.carId,locationString] isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        [PublicFunction showErrorMsg:jsonDic];
        
        StatisticModel *model = [[StatisticModel alloc]initWithaDic:jsonDic];
        //改变title
        [self setupNaviBarWithTitle:[NSString stringWithFormat:@"%@行车数据",model.date]];
        
        _statisticModel = model;
        
        self.itemData = @[@{@"title":@"今日里程",@"data":[NSString stringWithFormat:@"%@公里",model.mileage]},@{@"title":@"平均速度",@"data":[NSString stringWithFormat:@"%@km/h",model.avgSpeed]},@{@"title":@"最高速度",@"data":[NSString stringWithFormat:@"%@km/h",model.maxSpeed]},@{@"title":@"最低速度",@"data":[NSString stringWithFormat:@"%@km/h",model.minSpeed]}];
        
        [self.collectionView reloadData];
        //获取很多天的行车数据 异步 一定要请求完今天数据 再请求这个  刷新图表要用到今天的数据做判断（首先检查今天（对比时间）是否调用过15天的数据，如果没有就调用，反之不调用。然后调用当天数据；最后进行显示，并对时间进行格式化显示。）
        [self getSectionDateRequest];
        
        
    } fail:^{
        
    }];
    
}
#pragma -mark 将数据按照时间进行排序
- (void)sortTimeWithArr:(NSMutableArray *)arr{
    [arr sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        StatisticModel *model1 = obj1;  StatisticModel *model2 = obj2;
        NSComparisonResult result = [model1.date compare:model2.date];
        return result;
    }];
}
#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section
{
    return 4;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StatisticKMCell *cell =
    (StatisticKMCell *)[collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    
    cell.titleLa.text = self.itemData[indexPath.item][@"title"];
    cell.unitLa.text = self.itemData[indexPath.item][@"data"];
    cell.backView.userInteractionEnabled = YES;
    cell.iconImg.userInteractionEnabled = YES;
    //点击哪一行 标示哪一行
    if (self.nowRow == indexPath.row) {
        cell.iconImg.alpha = 1;
        cell.backView.hidden = NO;
    }else{
        cell.iconImg.alpha = 0;
        cell.backView.hidden = YES;
    }
    
    if (indexPath.row == 0) {
        cell.iconImg.image = [UIImage imageNamed:@"icon_line_small"];
    }else{
        cell.iconImg.image = [UIImage imageNamed:@"icon_bar_small"];
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self reloadPnchartWithIndexPath:indexPath];
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqual:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *filterHeaderView =
        [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                           withReuseIdentifier:kHeaderViewCellIdentifier
                                                  forIndexPath:indexPath];
        filterHeaderView.backgroundColor = [UIColor colorWithHexString:@"#FC5823"];//243 63 27  188 45 19
        for (UIView *v in filterHeaderView.subviews) {
            [v removeFromSuperview];
        }
        
        UILabel *line =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, kMainScreenWidth, 0.5)];
        line.backgroundColor = PNConGrayColor;
        [filterHeaderView addSubview:line];
        
        [filterHeaderView addSubview:self.baseScroll];
        
        
        
        return filterHeaderView;
    }
    return nil;
}
- (void)refreshSelectStatusAndShowWithIndexPath:(NSIndexPath *)indexPath{
    
    for (int i = 0; i < 4; i ++) {
        StatisticKMCell *cell = (StatisticKMCell *)[self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:indexPath.section]];
        cell.iconImg.alpha = 0;
        cell.backView.hidden = YES;
    }
    
    StatisticKMCell *showCell = (StatisticKMCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    showCell.iconImg.alpha = 1;
    showCell.backView.hidden = NO;
    
    self.nowRow = indexPath.row;
}
- (void)reloadPnchartWithIndexPath:(NSIndexPath *)indexPath{
    //刷新和展示点击item 展示的状态
    [self refreshSelectStatusAndShowWithIndexPath:indexPath];
    
    NSMutableArray *dateArr = [[NSMutableArray alloc]initWithCapacity:0];
    for (StatisticModel *model in self.headData) {
        NSString *str= [model.date substringFromIndex:5];
        //        NSString *newDateStr = [str stringByReplacingOccurrencesOfString:@"-" withString:@"/"];
        if ([model.date isEqualToString:_statisticModel.date]) {
            str = @"今天";
        }
        [dateArr addObject:str];
    }
    
    NSMutableArray *newDataArr = [[NSMutableArray alloc]initWithCapacity:0];
    //根据不同的item 刷新不同的数据
    switch (indexPath.item) {
        case 0:
        {
            for (StatisticModel *statistic in self.headData) {
                [newDataArr addObject:statistic.mileage];
            }
            break;
        }
        case 1:
        {
            for (StatisticModel *statistic in self.headData) {
                [newDataArr addObject:statistic.avgSpeed];
            }
            break;
        }
        case 2:
        {
            for (StatisticModel *statistic in self.headData) {
                [newDataArr addObject:statistic.maxSpeed];
            }
            break;
        }
        case 3:
        {
            for (StatisticModel *statistic in self.headData) {
                [newDataArr addObject:statistic.minSpeed];
            }
            break;
        }
        default:
            break;
    }
    if (indexPath.row == 0) {
        for (UIView *v in self.baseScroll.subviews) {
            if (v) {
                [v removeFromSuperview];
            }
        }
        NSLog(@"sub===%@",self.baseScroll.subviews);
        [self.baseScroll addSubview:self.lineChart];
        [self.lineChart setXLabels:dateArr];
        [self.lineChart setYLabels:newDataArr];
        
        PNLineChartData *data01 = [PNLineChartData new];
        //        data01.dataTitle = @"Alpha";
        data01.color = [UIColor blackColor];
        data01.alpha = 0.3f;
        data01.itemCount = newDataArr.count;
        data01.showPointLabel = YES;
        data01.pointLabelFont = [UIFont systemFontOfSize:8];
        data01.inflexionPointStyle = PNLineChartPointStyleCicleCenter;
        data01.getData = ^(NSUInteger index) {
            CGFloat yValue = [newDataArr[index] floatValue];
            return [PNLineChartDataItem dataItemWithY:yValue];
        };
        
        self.lineChart.chartData = @[data01];
        
        [self.lineChart strokeChart];
    }else{
        for (UIView *v in self.baseScroll.subviews) {
            if (v) {
                [v removeFromSuperview];
            }
        }
        NSLog(@"sub===%@",self.baseScroll.subviews);
        [self.baseScroll addSubview:self.barChart];
        [self.barChart setXLabels:dateArr];
        [self.barChart setYValues:newDataArr];
        
        NSMutableArray *strokeColors = [[NSMutableArray alloc]initWithCapacity:0];
        for (int i = 0; i < 15; i++) {
            
            [strokeColors addObject:PNAlphaGrayColor];
        }
        [self.barChart setStrokeColors:(NSArray *)strokeColors];
        
        [self.barChart strokeChart];
    }
    
    
}
- (PNLineChart *)getLineChart{
    PNLineChart *lineChart = [[PNLineChart alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*2, self.chartHeight)];
    lineChart.backgroundColor = [UIColor clearColor];
    [lineChart setXLabels:@[@"SEP 1",@"SEP 2",@"SEP 3",@"SEP 4",@"SEP 5",@"SEP 6",@"SEP 7"]];
    lineChart.showCoordinateAxis = YES;
    
    
    [lineChart strokeChart];
    lineChart.delegate = self;
    
    
    //    [self.view addSubview:self.lineChart];
    
    lineChart.legendStyle = PNLegendItemStyleStacked;
    lineChart.legendFont = [UIFont boldSystemFontOfSize:12.0f];
    lineChart.legendFontColor = [UIColor redColor];
    
    
    
    return lineChart;
}
- (PNBarChart *)getBarChart{
    
    PNBarChart *barChart = [[PNBarChart alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*2 , self.chartHeight)];
    //        self.barChart.center = CGPointMake(kMainScreenWidth/2, self.barChart.frame.origin.y + self.barChart.frame.size.height/2);
    //        self.barChart.backgroundColor = [UIColor blueColor];
    //                self.barChart.showLabel = NO;//指所有的lab
    barChart.backgroundColor = [UIColor clearColor];
    //        self.barChart.yLabelFormatter = ^(CGFloat yValue){
    //            return [barChartFormatter stringFromNumber:[NSNumber numberWithFloat:yValue]];
    //        };
    
    barChart.chartMarginLeft = 10.0;
    barChart.chartMarginRight = 10.0;
    barChart.chartMarginTop = 5.0;
    barChart.chartMarginBottom = 10.0;
    
    
    barChart.labelMarginTop = 5.0;
    //        self.barChart.showChartBorder = YES;
    //    [barChart setXLabels:@[@"2",@"3",@"4",@"5",@"2",@"3",@"4",@"5"]];
    //       self.barChart.yLabels = @[@-10,@0,@10];
    //        [self.barChart setYValues:@[@10000.0,@30000.0,@10000.0,@100000.0,@500000.0,@1000000.0,@1150000.0,@2150000.0]];
    //    [barChart setYValues:@[@10.82,@1.88,@6.96,@50,@10.82,@1.88,@6.96,@33.93]];
    [barChart setStrokeColors:@[PNAlphaGrayColor,PNAlphaGrayColor,PNAlphaGrayColor,PNAlphaGrayColor,PNAlphaGrayColor,PNAlphaGrayColor,PNAlphaGrayColor,PNAlphaGrayColor]];
    barChart.isGradientShow = NO;
    barChart.showLevelLine = NO;
    barChart.isShowNumbers = YES;
    barChart.labelTextColor = [UIColor whiteColor];
    barChart.barBackgroundColor = [UIColor clearColor];
    
    [barChart strokeChart];
    
    barChart.delegate = self;
    
    return barChart;
    
}

#pragma mark - UICollectionViewDelegateLeftAlignedLayout
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(kMainScreenWidth/2, 80);
}
#pragma -mark item之间水平间距
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}
#pragma -mark item之间竖直间距
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
referenceSizeForHeaderInSection:(NSInteger)section
{
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, self.chartHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}
//- (float)collectionCellWidthText:(NSString *)text content:(NSDictionary *)content{
//    float cellWidth;
//    CGSize size = [text sizeWithAttributes:
//                   @{NSFontAttributeName:
//                         [UIFont systemFontOfSize:15]}];
//    cellWidth = size.width;
//
//    return cellWidth +30;
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark clickPnchartBar
- (void)userClickedOnBarAtIndex:(NSInteger)barIndex
{
    
    NSLog(@"Click on bar %@", @(barIndex));
    StatisticModel *model = self.headData[barIndex];
    //改变title
    [self setupNaviBarWithTitle:[NSString stringWithFormat:@"%@行车数据",model.date]];
    self.itemData = @[@{@"title":@"今日里程",@"data":[NSString stringWithFormat:@"%@公里",model.mileage]},@{@"title":@"平均速度",@"data":[NSString stringWithFormat:@"%@km/h",model.avgSpeed]},@{@"title":@"最高速度",@"data":[NSString stringWithFormat:@"%@km/h",model.maxSpeed]},@{@"title":@"最低速度",@"data":[NSString stringWithFormat:@"%@km/h",model.minSpeed]}];
    
    [self.collectionView reloadData];
    
    PNBar * bar = [self.barChart.bars objectAtIndex:barIndex];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    animation.fromValue = @1.0;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.toValue = @1.1;
    animation.duration = 0.2;
    animation.repeatCount = 0;
    animation.autoreverses = YES;
    animation.removedOnCompletion = YES;
    animation.fillMode = kCAFillModeForwards;
    
    [bar.layer addAnimation:animation forKey:@"Float"];
}
- (void)userClickedOnLineKeyPoint:(CGPoint)point lineIndex:(NSInteger)lineIndex pointIndex:(NSInteger)pointIndex{
    NSLog(@"Click Key on line %f, %f line index is %d and point index is %d",point.x, point.y,(int)lineIndex, (int)pointIndex);
}

- (void)userClickedOnLinePoint:(CGPoint)point lineIndex:(NSInteger)lineIndex{
    NSLog(@"Click on line %f, %f, line index is %d",point.x, point.y, (int)lineIndex);
}
#pragma mark -UIButtonEvent
- (void)leftBtnAction
{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
    }];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
