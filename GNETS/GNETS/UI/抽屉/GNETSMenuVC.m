//
//  GNETSMenuVC.m
//  GNETS
//
//  Created by cqz on 16/4/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "GNETSMenuVC.h"
#import "CarDataVC.h"
#import "CarLoInfo.h"
#import "MineDataCell.h"
#import "MineInfoCell.h"
#import "ViewController.h"
#import "BasicDataVC.h"
#import "ClauseVC.h"
#import "AboutVC.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerController.h"

@interface GNETSMenuVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,retain)NSArray *data;
@property (nonatomic, retain)UITableView *mineTable;
@property (nonatomic, retain)CarLoInfo *carInfo;

@end

@implementation GNETSMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:kMainScreenFrameRect];
    imageView.userInteractionEnabled = YES;
    imageView.image = [UIImage imageNamed:@"bg_myPage"];
    [self.view addSubview:imageView];
    
    [self initUI];
}
- (void)initUI
{
    self.mineTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 52, leftWidth, kMainScreenHeight - 52) style:UITableViewStylePlain];
    self.mineTable.delegate = self;
    self.mineTable.dataSource = self;
    self.mineTable.bounces = NO;
    self.mineTable.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.mineTable];
    self.mineTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.mineTable registerNib:[UINib nibWithNibName:@"MineDataCell" bundle:nil] forCellReuseIdentifier:@"dataCell"];
    
    [self.mineTable registerNib:[UINib nibWithNibName:@"MineInfoCell" bundle:nil] forCellReuseIdentifier:@"infoCell"];
    
    self.data = @[@[@{@"icon":@"menu_user_info",@"title":@"基本资料"}],@[@{@"icon":@"menu_user_info",@"title":@"基本资料"},@{@"icon":@"menu_car_info",@"title":@"车辆资料"},@{@"icon":@"menu_service_terms",@"title":@"服务条款"},@{@"icon":@"menu_contact_service",@"title":@"联系客服"},@{@"icon":@"menu_about",@"title":@"关于"},@{@"icon":@"menu_exit_user",@"title":@"退出账号"}]];
}
#pragma mark - table
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 83;
    }
    return 45;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] init];
    headView.backgroundColor = [UIColor clearColor];
    return headView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    return 6;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1) {
        return 15.f;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MineDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell"];
        cell.nameLa.text = [[PublicFunction ShareInstance] getAccount].data.userName;
        cell.DeviceNo.text = [NSString stringWithFormat:@"%@",[[PublicFunction ShareInstance]getAccount].data.carId];
        cell.headImg.image = [UIImage imageNamed:@"meun_gaksee_logo"];
        return cell;
    }
    
    MineInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"infoCell"];
    
    NSArray *datas = [self.data objectAtIndex:indexPath.section];
    NSDictionary *info = datas[indexPath.row];
    cell.titleLa.text = info[@"title"];
    cell.headImg.image = [UIImage imageNamed:info[@"icon"]];
        
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    NSUInteger section = indexPath.section;
    NSUInteger row = indexPath.row;
    
    if (section == 1)
    {
        switch (row) {
            case 0:
            {//基本资料
                BasicDataVC *vc = [[BasicDataVC alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                [self presentViewController:vc animated:YES completion:nil];
            }
                break;
            case 1:
            {//车辆资料
                CarDataVC *vc = [[CarDataVC alloc]init];
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
                vc.hidesBottomBarWhenPushed = YES;
                [self presentViewController:nav animated:YES completion:nil];
            }
                break;
            case 2:
            {
                //服务条款
                ClauseVC *vc = [[ClauseVC alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                vc.claTitle = @"服务条款";
                vc.urlString = Service_Terms;
                [self presentViewController:vc animated:YES completion:nil];
            }
                break;
            case 3:
            {//联系客服
                NSMutableString * str=[[NSMutableString alloc] initWithFormat:@"telprompt://%@",@"0531-67805000"];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];

            }
                break;
            case 4:
            {//关于
                AboutVC *vc = [[AboutVC alloc]init];
                vc.hidesBottomBarWhenPushed = YES;
                [self presentViewController:vc animated:YES completion:nil];
            }
                break;
            case 5:
            {
                UIWindow *windows = [ConFunc getWindow];
                MMDrawerController *mmvc = (MMDrawerController *)windows.rootViewController;

                [self.mm_drawerController
                                     setCenterViewController:mmvc.centerViewController
                                     withFullCloseAnimation:YES
                                     completion:nil];

                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"您确定要退出登录吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
                alert.delegate = self;
                [alert show];

                
            }
                break;

                
            default:
                break;
        }
        
    }

}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
            [SVProgressHUD showWithStatus:@"正在退出登录.."];
            [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,Logout,[[PublicFunction ShareInstance]getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
                [SVProgressHUD dismiss];
                if ([jsonDic[@"errmsg"] isEqualToString:@"ok"]) {
                    [USER_D removeObjectForKey:@"user_phone"];
                    [USER_D removeObjectForKey:@"user_password"];
                    [USER_D removeObjectForKey:@"WARNMSG"];
                    [USER_D removeObjectForKey:@"UNREAD"];
                    [USER_D synchronize];
                    [PublicFunction ShareInstance].m_user.data.province = nil;
                    ViewController *v = [[ViewController alloc]init];
                    [v isAutoLogin];
                    
                }else if ([jsonDic[@"code"] isEqualToString:@"0"]){
                    [SVProgressHUD showErrorWithStatus:jsonDic[@"errmsg"]];
                }
                
            } fail:^{
                
            }];
        }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
