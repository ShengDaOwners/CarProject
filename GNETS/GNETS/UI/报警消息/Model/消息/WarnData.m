//
//  data.m
//  消息列表
//
//  Created by _author on 16-03-06.
//  Copyright (c) _companyname. All rights reserved.
//  

/*
	
*/


#import "WarnData.h"
#import "DTApiBaseBean.h"
#import "MJExtension.h"


@implementation WarnData

@synthesize carId = _carId;
@synthesize createDate = _createDate;
@synthesize createTime = _createTime;
@synthesize eventId = _eventId;
@synthesize eventType = _eventType;
@synthesize heading = _heading;
@synthesize lat = _lat;
@synthesize lon = _lon;
@synthesize msg = _msg;
@synthesize sourceId = _sourceId;
@synthesize speed = _speed;
@synthesize status = _status;
@synthesize statusDate = _statusDate;

-(id)initWithDictionary:(NSDictionary*)dict
{
    if (self = [super init])
    {
		DTAPI_DICT_ASSIGN_NUMBER(carId, @"0");
		DTAPI_DICT_ASSIGN_STRING(createDate, @"");
		DTAPI_DICT_ASSIGN_NUMBER(createTime, @"0");
		DTAPI_DICT_ASSIGN_NUMBER(eventId, @"0");
		DTAPI_DICT_ASSIGN_NUMBER(eventType, @"0");
		DTAPI_DICT_ASSIGN_NUMBER(heading, @"0");
		DTAPI_DICT_ASSIGN_NUMBER(lat, @"0");
		DTAPI_DICT_ASSIGN_NUMBER(lon, @"0");
		DTAPI_DICT_ASSIGN_STRING(msg, @"");
		DTAPI_DICT_ASSIGN_NUMBER(sourceId, @"0");
		DTAPI_DICT_ASSIGN_NUMBER(speed, @"0");
		DTAPI_DICT_ASSIGN_NUMBER(status, @"0");
		DTAPI_DICT_ASSIGN_STRING(statusDate, @"");
    }
    
    return self;
}

-(NSDictionary*)dictionaryValue
{
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    
	DTAPI_DICT_EXPORT_BASICTYPE(carId);
	DTAPI_DICT_EXPORT_BASICTYPE(createDate);
	DTAPI_DICT_EXPORT_BASICTYPE(createTime);
	DTAPI_DICT_EXPORT_BASICTYPE(eventId);
	DTAPI_DICT_EXPORT_BASICTYPE(eventType);
	DTAPI_DICT_EXPORT_BASICTYPE(heading);
	DTAPI_DICT_EXPORT_BASICTYPE(lat);
	DTAPI_DICT_EXPORT_BASICTYPE(lon);
	DTAPI_DICT_EXPORT_BASICTYPE(msg);
	DTAPI_DICT_EXPORT_BASICTYPE(sourceId);
	DTAPI_DICT_EXPORT_BASICTYPE(speed);
	DTAPI_DICT_EXPORT_BASICTYPE(status);
	DTAPI_DICT_EXPORT_BASICTYPE(statusDate);
    return md;
}
MJCodingImplementation
@end
