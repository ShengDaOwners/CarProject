//
//  WarningMsgVC.m
//  GNETS
//
//  Created by tcnj on 16/2/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "WarningMsgVC.h"
#import "WarningCell.h"
#import "MJRefresh.h"
#import "WarnRoot.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerController.h"
#import "CollectEmptyView.h"


static NSString *const WARNINGIDENTIFER = @"warningIdentifer";

@interface WarningMsgVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSUInteger _indexCount;//刷新 1  更多 2
    NSUInteger _currentRow;//当前点击行

    NSString *_maxString;
    NSString *_minString;
    NSUInteger _unread;//未读消息个数
    
    CGFloat _cellHeight;
}
@property (nonatomic,strong) CollectEmptyView *emptyView;//收藏为空时候

@property (nonatomic,strong)UITableView *tableView;//表
@property (nonatomic,strong)NSMutableArray *dataArrays;//数据源
@property (nonatomic ,copy) NSString *markStr;//查询标识，刷新传1加载更多传2
@property (nonatomic ,copy) NSString *eventId;//报警消息编号
@property (nonatomic,copy) NSString *warnUrlString;

@end

@implementation WarningMsgVC
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[SoundManager sharedSoundManager] musicStop];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNaviBarWithBtn:NaviLeftBtn title:nil img:@"icon_title_user"];

    
    _markStr = @"1";
    _maxString = @"0";
    _minString = @"0";
    _unread = 0;
    _cellHeight = 60;
    NSString *unread = [USER_D objectForKey:@"UNREAD"];
    if (unread.length >0 ) {
        _unread = [unread integerValue];
    }

    _currentRow = -1;
    [self initView];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    NSUInteger unread = 0;
    NSMutableArray *arr = [NSMutableArray array];
    for (WarnData *datamodel in _dataArrays) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:datamodel];
        if (datamodel.isLook == NO) {
            unread +=1;
        }
        [arr addObject:data];
    }
    [USER_D setObject:[NSString stringWithFormat:@"%ld",unread] forKey:@"UNREAD"];
    //存储自定义对象
    [USER_D setObject:arr forKey:@"WARNMSG"];
}
- (void)initView{
    [self setupNaviBarWithTitle:@"报警消息"];
    float height = self.view.frame.size.height-64;
    self.emptyView = [[CollectEmptyView alloc] initWithFrame:CGRectMake(0, 64, kMainScreenWidth, height) WithText:@"暂无新报警消息"];

    //获取数据
    self.dataArrays = [NSMutableArray array];
    NSMutableArray *tempArrays = [NSMutableArray array];
    
    tempArrays = [USER_D objectForKey:@"WARNMSG"];
    for (NSData *data in tempArrays) {
        WarnData *dataModel = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        [_dataArrays addObject:dataModel];
    }
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight-64.f) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.view addSubview:_tableView];
    [_tableView registerNib:[UINib nibWithNibName:@"WarningCell" bundle:nil] forCellReuseIdentifier:WARNINGIDENTIFER];
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(upRefresh:)];
    [header setTitle:@"数据加载中..." forState:MJRefreshStateRefreshing];
    header.stateLabel.font = Font_14;
    header.lastUpdatedTimeLabel.font = Font_13;
    header.backgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
    // 设置颜色
    header.stateLabel.textColor = [UIColor grayColor];
    header.lastUpdatedTimeLabel.textColor = [UIColor grayColor];
    //MJRefreshNormalHeader
    self.tableView.header = header;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(downRefresh:)];
    // 设置字体
    footer.stateLabel.font = Font_14;
    footer.stateLabel.textColor =[UIColor grayColor];
    footer.backgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
    //MJRefreshBackNormalFooter MJRefreshAutoNormalFooter
    
    self.tableView.footer = footer;
    // 马上进入刷新状态
    [self.tableView.header beginRefreshing];
}
#pragma mark ------ 下拉刷新
- (void)upRefresh:(id)sender
{
    if (_dataArrays.count>0) {
        [self  CalculateTheBiggestAndSmall];
    }

    _markStr = @"1";
   _warnUrlString = [NSString stringWithFormat:@"%@%@?carId=%@&eventId=%@&mark=%@",kProjectBaseUrl,AlarmEventInfo,[PublicFunction ShareInstance].getAccount.data.carId,_maxString,_markStr];

    //加个旋转框
    [SVProgressHUD showWithStatus:@"加载中..." maskType:SVProgressHUDMaskTypeClear];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self loadData];
        
    });
}
#pragma mark -算出最大和最小的 eventId
- (void)CalculateTheBiggestAndSmall
{
    NSMutableArray *arrays = [NSMutableArray array];
    for (WarnData *data in _dataArrays)
    {
        NSString *eventS = [NSString stringWithFormat:@"%@",data.eventId];
        [arrays addObject:eventS];
    }
    NSArray *comArray = [arrays sortedArrayUsingSelector:@selector(compare:)];
    _maxString = [comArray lastObject];
    _minString = [comArray firstObject];
}
#pragma mark ------ 上拉加载
- (void)downRefresh:(id)sender
{
    if (_dataArrays.count>0) {
    [self  CalculateTheBiggestAndSmall];
    }
    _markStr = @"2";
    //加个旋转框
    _warnUrlString = [NSString stringWithFormat:@"%@%@?carId=%@&eventId=%@&mark=%@",kProjectBaseUrl,AlarmEventInfo,[PublicFunction ShareInstance].getAccount.data.carId,_minString,_markStr];

    [SVProgressHUD showWithStatus:@"加载中..." maskType:SVProgressHUDMaskTypeClear];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [self loadData];
        
    });
}
#pragma mark -请求
- (void)loadData{
    
    [GNETS_NetWorkManger GetJSONWithUrl:[_warnUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] isNeedHead:YES success:^(NSDictionary *jsonDic) {
        [SVProgressHUD dismiss];
        [self.tableView.header endRefreshing];
        [self.tableView.footer endRefreshing];

        WarnRoot *model = [[WarnRoot alloc] initWithDictionary:jsonDic];
        if ([model.code intValue] ==1 || model.data.count>0) {
            
            for (NSUInteger i =0; i<model.data.count; i++) {

                WarnData *dataModel = model.data[0];
                if (![_dataArrays containsObject:dataModel]) {
                    
                    [_dataArrays addObject:dataModel];
                    _unread +=1;
                }
            }
            [_tableView reloadData];
            
            UITabBarItem *item = self.tabBarController.tabBar.items[1];
            [item setBadgeValue:[NSString stringWithFormat:@"%ld",_unread]];
        }else{
            [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
            if ([_markStr isEqualToString:@"2"]) {
                [self.tableView.footer endRefreshing];
            }
        }
    } fail:^{
        [SVProgressHUD dismiss];
        [self.tableView.header endRefreshing];
        [self.tableView.footer endRefreshing];

    }];
        
}
#pragma mark -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.dataArrays.count ==0) {
        [self.view addSubview:self.emptyView];
        [self.view bringSubviewToFront:self.tableView];
        
    }else{
        if (self.emptyView) {
            [self.emptyView removeFromSuperview];
        }
    }
    return _dataArrays.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WarningCell *cell = (WarningCell *)[tableView dequeueReusableCellWithIdentifier:WARNINGIDENTIFER];
   // cell.expanded  = _flag[indexPath.row];
    cell.row = indexPath.row;
    cell.currentRow = _currentRow;
    if (_dataArrays.count >0) {
        [cell setModel:_dataArrays[_dataArrays.count-1-indexPath.row]];
    }
    UIView *lineView = [[UIView alloc] init];
    lineView.tag = 3000;
    lineView.backgroundColor = RGBACOLOR(235, 235, 236, 1);
    [cell.contentView addSubview:lineView];
    if (indexPath.row == _currentRow) {
        lineView.frame = CGRectMake(0, _cellHeight - 0.5, kMainScreenWidth, 0.5);
    }else{
        lineView.frame = CGRectMake(0, 60 - 0.5, kMainScreenWidth, 0.5);
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WarningCell *cell = (WarningCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    _cellHeight = cell.rowHeight;
    _currentRow = indexPath.row;
    [_tableView  reloadData];
    
    WarnData *model = _dataArrays[_currentRow];
    if (model.isLook == NO) {
        NSString *eventId = [NSString stringWithFormat:@"%@",model.eventId];
        [NetWorkMangerTools lookViewAlarmWithEventId:eventId];
        model.isLook = YES;
        _unread = _unread-1;
        [_dataArrays replaceObjectAtIndex:_currentRow withObject:model];
    }
    
    UITabBarItem *item = self.tabBarController.tabBar.items[1];
    if (_unread<=0) {
        _unread = 0;
        [item setBadgeValue:nil];
    }else{
        [item setBadgeValue:[NSString stringWithFormat:@"%ld",_unread]];
    }
  
    //[_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:_currentRow inSection:0],nil] withRowAnimation:UITableViewRowAnimationNone];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    WarningCell *cell = (WarningCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.row == _currentRow) {
        
        return cell.rowHeight;
    }
    return 60;
}
#pragma mark -UIButtonEvent
- (void)leftBtnAction
{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
