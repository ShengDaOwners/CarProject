//
//  WarningCell.m
//  GNETS
//
//  Created by cqz on 16/2/27.
//  Copyright © 2016年 CQZ. All rights reserved.
//
#define SCREENWIDTH  [UIScreen mainScreen].bounds.size.width
#define SCREENHEIGHT  [UIScreen mainScreen].bounds.size.height

static float const DefaultCellHeight = 60;

#import "WarningCell.h"
#import "NSDate+Category.h"

@implementation WarningCell
- (void)awakeFromNib {
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
     _rowHeight = 0;
    
    _msgLab = [[UILabel alloc] init];
    _msgLab.numberOfLines = 0;
    _msgLab.textColor = [UIColor grayColor];
    _msgLab.font = [UIFont systemFontOfSize:14];
    _msgLab.lineBreakMode =  NSLineBreakByTruncatingTail;;
    [self.contentView addSubview:_msgLab];
    
}
- (void)setModel:(WarnData *)model
{
    _model = model;
    [self  loadData];
}
- (void)loadData{
    
    CGFloat labelMaxWidth = SCREENWIDTH-85;

    NSDictionary *attribute = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
    ziTiSize = [_model.msg boundingRectWithSize:CGSizeMake(labelMaxWidth, 9999)options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    _msgLab.text = _model.msg;
    
    CGFloat height = ziTiSize.height + 30 +10;
    
    DLog(@"高度是－－－－%f",height);
    
    if (height<DefaultCellHeight) {
        _rowHeight = 60;
    }else{
        _rowHeight = 10 + ziTiSize.height +28;
    }
    if (_currentRow == _row) {
        
        _msgLab.frame = CGRectMake(70, 30, labelMaxWidth, _rowHeight -35);
    }else{
        _msgLab.frame = CGRectMake(70, 30, labelMaxWidth, 20);

    }
    
    NSUInteger eventType = [_model.eventType integerValue];
    if (eventType == 1) {
        _titLab.text = @"震动报警";
        _warnImgView.image = [UIImage imageNamed:@"alarm_shake"];
    }else if (eventType == 2){
        _titLab.text = @"电子围栏";
        _warnImgView.image = [UIImage imageNamed:@"alarm_fence"];

    }else if (eventType == 3){
        _titLab.text = @"锁车通知";
        _warnImgView.image = [UIImage imageNamed:@"notify_lock"];
        
    }else if (eventType == 4){
        _titLab.text = @"解锁通知";
        _warnImgView.image = [UIImage imageNamed:@"notify_unlock"];

    }
    
    
    
    switch (eventType) {
        case 1://震动报警
            _titLab.text = @"震动报警";
            _warnImgView.image = [UIImage imageNamed:@"alarm_shake"];

            break;
        case 2://电子围栏
            _titLab.text = @"电子围栏";
            _warnImgView.image = [UIImage imageNamed:@"alarm_fence"];

            break;
        case 3://锁车通知
            _titLab.text = @"锁车通知";
            _warnImgView.image = [UIImage imageNamed:@"notify_lock"];

            break;
        case 4://解锁通知
            _titLab.text = @"解锁通知";
            _warnImgView.image = [UIImage imageNamed:@"notify_unlock"];

            break;
        case 11://电池欠压
            _titLab.text = @"解锁通知";
            _warnImgView.image = [UIImage imageNamed:@"alarm_lowpower"];

            break;
        case 12://异常移动
            _titLab.text = @"异常移动";
            _warnImgView.image = [UIImage imageNamed:@"alarm_move"];

            break;
        case 13://主电源切断
            _titLab.text = @"主电源切断";
            _warnImgView.image = [UIImage imageNamed:@"alarm_power"];

            break;
        case 21://保险通知
            _titLab.text = @"保险通知";
            _warnImgView.image = [UIImage imageNamed:@"notify_insur"];

            break;
        case 22://注册通知
            _titLab.text = @"注册通知";
            _warnImgView.image = [UIImage imageNamed:@"notify_register"];
            break;
        case 23://通知消息
            _titLab.text = @"通知消息";
            _warnImgView.image = [UIImage imageNamed:@"notify_msg"];

            break;
    }
    
    NSString *timeS = [NSString stringWithFormat:@"%@",_model.createTime];
    _timeLab.text =[self changeTime:[timeS doubleValue]/1000];

}

- (NSString *)changeTime:(NSTimeInterval)time
{
//    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
//    //time+28800
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:time];
    
    NSString *dateString = [date timeOftenUser];
//   // NSString *nowString = [formatter stringFromDate:date];
//    
//    if ([[ConFunc compareDate:date] isEqualToString:@"今天"]) {
//        
//        NSCalendar *calendar = [NSCalendar currentCalendar];
//        NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
//        NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:date];
//        
//        NSUInteger hour = [dateComponent hour];
//        NSUInteger minute = [dateComponent minute];
//        
//        NSString *jintianStr = [NSString stringWithFormat:@"今天 %ld:%ld",hour,minute];
//        return jintianStr;
//    }else if ([[ConFunc compareDate:date] isEqualToString:@"昨天"]) {
//        
//        NSCalendar *calendar = [NSCalendar currentCalendar];
//        NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
//        NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:date];
//        
//        NSUInteger hour = [dateComponent hour];
//        NSUInteger minute = [dateComponent minute];
//        
//        NSString *zuoTStr = [NSString stringWithFormat:@"昨天 %ld:%ld",hour,minute];
//        return zuoTStr;
//    }
//    
//    NSString* dateString = [formatter stringFromDate:date];
//    DLog(@"时间－－－－－%@",dateString);
    return dateString;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
