//
//  WarningCell.h
//  GNETS
//
//  Created by cqz on 16/2/27.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WarnData.h"
@protocol WarningCellPro <NSObject>

- (void)expandOrClose:(UITableViewCell *)cell;

@end

@interface WarningCell : UITableViewCell
{
     CGSize ziTiSize;
}

@property (nonatomic,weak)id<WarningCellPro>delegate;//代理
@property (nonatomic,strong) UIButton *showAllButton;
@property (nonatomic,assign) BOOL isExpandable;    // 是否显示"收起"按钮
@property (nonatomic,assign) BOOL expanded;     // 收起或展开操作
@property (nonatomic,assign) CGFloat rowHeight;//高度
@property (strong, nonatomic)  UILabel *msgLab;
@property (strong, nonatomic) WarnData *model;
@property (assign, nonatomic) NSUInteger currentRow;
@property (assign, nonatomic) NSUInteger row;

@property (weak, nonatomic) IBOutlet UIImageView *warnImgView;
@property (weak, nonatomic) IBOutlet UILabel *titLab;
@property (weak, nonatomic) IBOutlet UILabel *timeLab;

@end
