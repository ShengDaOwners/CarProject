//
//  CollectEmptyView.h
//  ZhouDao
//
//  Created by apple on 16/3/14.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectEmptyView : UIView
- (id)initWithFrame:(CGRect)frame WithText:(NSString *)textStr;
@end
