//
//  InstallViewController.m
//  GNETS
//
//  Created by cqz on 16/3/2.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "InstallViewController.h"
#import "MyPickView.h"

#define VIEWHEIGHT   (kMainScreenWidth-300.f)/3.f

static NSString *const  INSTALLID = @"installIdentifer";
@interface InstallViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    NSString *_name;NSString *_phone; NSString *_provice; NSString *_city;
    NSString *_area; NSString *_address;NSString *_num; NSString *_carid;
    
    NSString *_tempStr;//拼接省市区
    UITapGestureRecognizer * _tapGesture;

}
@property (nonatomic,strong) UITableView *tableView;
//@property (nonatomic,strong) UIImageView *imgView1;
//@property (nonatomic,strong) UIImageView *imgView2;
//@property (nonatomic,strong) UIImageView *imgView3;

@property (nonatomic,strong) NSMutableArray *titleArrays;//标题数组
@end

@implementation InstallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    
    [self initView];
}
- (void)initView
{
    
//    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, kMainScreenHeight -120 , kMainScreenWidth, .5f)];
//    lineView.backgroundColor = [UIColor lightGrayColor];
//    [self.view addSubview:lineView];
//    
//    _imgView1 = [[UIImageView alloc] initWithFrame:CGRectMake(VIEWHEIGHT/2.f, kMainScreenHeight - 110, 100, 100)];
//    _imgView1.image = [UIImage imageNamed:@"code_alipay"];
//    [self.view addSubview:_imgView1];
//    _imgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(VIEWHEIGHT*3.f/2.f +100, kMainScreenHeight - 110, 100, 100)];
//    _imgView2.image = [UIImage imageNamed:@"code_weixin"];
//    [self.view addSubview:_imgView2];
//    _imgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(VIEWHEIGHT/2.f + 2*VIEWHEIGHT +200, kMainScreenHeight - 110, 100, 100)];
//    _imgView3.image = [UIImage imageNamed:@"code_account"];
//    [self.view addSubview:_imgView3];
//
    
    
    _name = @"";_phone = @"";_provice= @"";_city = @"";_area = @"";_address = @"";_num = @"";_carid = @"";
    _tempStr = @"";
    [self setupNaviBarWithTitle:@"预约安装"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:@"登录" img:@"back_icon"];
    [self setupNaviBarWithBtn:NaviRightBtn title:@"提交" img:nil];
    
    _titleArrays = [NSMutableArray arrayWithObjects:@"您的姓名",@"联系电话",@"所在地区",@"详细地址",@"所需数量", nil];
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 84, kMainScreenWidth, 220) style:UITableViewStylePlain];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.scrollEnabled = NO;
    _tableView.backgroundColor = [UIColor clearColor];
    [_tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    [self.view addSubview:_tableView];
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:INSTALLID];
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    _tapGesture.cancelsTouchesInView = NO;//关键代码
    [self.tableView addGestureRecognizer:_tapGesture];

}
#pragma mark -手势
- (void)dismissKeyBoard{
    [self.view endEditing:YES];
}
#pragma amrk -UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:INSTALLID];
    cell.textLabel.text = _titleArrays[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = Font_16;

    if (![cell.textLabel.text isEqualToString:@"所需数量"])
    {
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 43.5, kMainScreenWidth -15, .5)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#d7d7d7"];
        [cell.contentView addSubview:lineView];
    }

    NSUInteger row = indexPath.row;

    if (row ==2) {
        UILabel *label = (UILabel *)[self.view viewWithTag:3383];
        [label removeFromSuperview];
        UILabel *lab = [[UILabel alloc] initWithFrame:CGRectMake(kMainScreenWidth -215, 12, 180, 20)];
        lab.textColor = [UIColor blackColor];
        _tempStr.length>0?[lab setText:_tempStr]:[lab setText:@"必选"];
        lab.textAlignment = NSTextAlignmentRight;
        lab.font = Font_14;
        lab.tag = 3383;
        [cell.contentView addSubview:lab];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }else{
        UITextField *textF = [[UITextField alloc] initWithFrame:CGRectMake(kMainScreenWidth -195, 12, 180, 20)];
        textF.tag = 5000+row;
        textF.backgroundColor = [UIColor clearColor];
        textF.borderStyle = UITextBorderStyleNone;
        textF.textAlignment = NSTextAlignmentRight;
        textF.font = Font_14;
        textF.placeholder = @"必填";
        [textF setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
        [textF setValue:Font_14 forKeyPath:@"_placeholderLabel.font"];

        textF.delegate = self;
        [cell.contentView addSubview:textF];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:textF];

    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyBoard];
    
    if (indexPath.row == 2) {
        
        MyPickView *pickView = [[MyPickView alloc] initWithFrame:CGRectMake(0, 0, kMainScreenWidth, kMainScreenHeight)];
        pickView.pickBlock = ^(NSString *proviceStr,NSString *cityStr,NSString *districtlistStr){
            
            DLog(@"%@－%@－%@",proviceStr,cityStr,districtlistStr);
            _provice = proviceStr;_city = cityStr; _area = districtlistStr;
            NSString *tempStr = [NSString stringWithFormat:@"%@-%@-%@",proviceStr,cityStr,districtlistStr];
            _tempStr = tempStr;
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:2 inSection:0],nil] withRowAnimation:UITableViewRowAnimationNone];
            
        };
        [self.view addSubview:pickView];
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag ==5001 || textField.tag ==5004) {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    return YES;
}
- (void)textFieldChanged:(NSNotification*)noti{
    
    UITextField *textField = (UITextField *)noti.object;
    BOOL flag=[NSString isContainsTwoEmoji:textField.text];
    if (flag){
        SHOW_ALERT(@"不能输入表情!");
        textField.text = [NSString disable_emoji:textField.text];
    }
    

    NSUInteger index = textField.tag;
    
    if (index == 5000) {
        _name = textField.text;
    }else if (index == 5001){
        _phone = textField.text;
    }else if (index == 5003){
        _address = textField.text;
    }else if (index == 5004){
        _num = textField.text;
    }
    
}
#pragma mark -UIButtonEvent
- (void)rightBtnAction
{
    DLog(@"提交");
    if (_name.length<=0 && _phone.length<=0 && _provice.length<=0 && _city.length<=0 && _area.length<=0 && _address.length<=0 && _num.length<=0 && _carid.length<=0) {
        SHOW_ALERT(@"请您检查您的资料");
        return;
    }
    
    //加个旋转框
    [SVProgressHUD showWithStatus:@"正在提交..." maskType:SVProgressHUDMaskTypeClear];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObjectWithNullValidate:_name forKey:@"name"];
    [dict setObjectWithNullValidate:_phone forKey:@"phone"];
    [dict setObjectWithNullValidate:_provice forKey:@"province"];
    [dict setObjectWithNullValidate:_city forKey:@"city"];
    [dict setObjectWithNullValidate:_area forKey:@"area"];
    [dict setObjectWithNullValidate:_address forKey:@"address"];
    [dict setObjectWithNullValidate:_num forKey:@"num"];
//    [dict setObjectWithNullValidate:_carid forKey:@"carId"];
    
    NSString *urlstr = [NSString stringWithFormat:@"%@%@",kProjectBaseUrl,ONLINEBOOK];
    [GNETS_NetWorkManger PostJSONWithUrl:urlstr parameters:dict isNeedHead:NO success:^(NSDictionary *jsonDic) {
        [SVProgressHUD dismiss];
        NSUInteger code = [[jsonDic objectForKey:@"code"] integerValue];
        if (code == 1) {
            [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
            [self.navigationController popViewControllerAnimated:NO];
        }
        
    } fail:^{
        [SVProgressHUD dismiss];
    }];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self dismissKeyBoard];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
