//
//  RegisterViewController.h
//  GNETS
//
//  Created by tcnj on 16/2/19.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface RegisterViewController : BaseViewController

@property (nonatomic, assign) BOOL isMan;

@property (nonatomic,copy) NSString *imeiStr;
@property (nonatomic,copy) NSString *carid;
@property (nonatomic,copy) NSString *telNum;
@property (nonatomic,copy) NSString *userName;
//@property (nonatomic,copy) NSString *idNum;
@property (nonatomic,copy) NSString *phone;
@property (nonatomic,copy) NSString *province;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *area;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *dealerId;
@property (nonatomic,copy) NSString *salesman;
@property (nonatomic,copy) NSString *sex;

@end
