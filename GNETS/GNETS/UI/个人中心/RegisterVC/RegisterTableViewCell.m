//
//  RegisterTableViewCell.m
//  GNETS
//
//  Created by tcnj on 16/2/19.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "RegisterTableViewCell.h"

@implementation RegisterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;

        self.titleLab = [[UILabel alloc] init];
        self.titleLab.textAlignment = NSTextAlignmentLeft;
        self.titleLab.font = Font_16;
        [self.contentView addSubview:self.titleLab];
        
        self.deviceLabel = [[UILabel alloc] init];
        self.deviceLabel.textAlignment = NSTextAlignmentRight;
        self.deviceLabel.font = Font_14;
        self.deviceLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.deviceLabel];
        
        self.sexImgView = [[UIImageView alloc] init];
        self.sexImgView.image = [UIImage imageNamed:@"icon_sex_man"];
        self.sexImgView.userInteractionEnabled = YES;
        [self.contentView addSubview:self.sexImgView];
        
        self.textField = [[UITextField alloc] init];
        self.textField.backgroundColor = [UIColor clearColor];
        self.textField.borderStyle = UITextBorderStyleNone;
        self.textField.textAlignment = NSTextAlignmentRight;
        self.textField.font = Font_14;
        [self.contentView addSubview:self.textField];

        _lineView = [[UIView alloc] initWithFrame:CGRectMake(15, 43.5, [UIScreen mainScreen].bounds.size.width-15 , .5)];
        _lineView.backgroundColor = [UIColor colorWithHexString:@"#d7d7d7"];//RGBACOLOR(235, 235, 236, 1);//colorWithHexString:@"#FFA043"];
        [self.contentView addSubview:_lineView];
        
    }
    return self;
}
- (void)layoutSubviews
{
    self.titleLab.frame = CGRectMake(15, 12, 120, 20);
//    if (self.sectionIndex == 1 && self.rowIndex == 1) {
//        self.sexImgView.hidden = NO;
//        self.sexImgView.frame = CGRectMake(kMainScreenWidth - 65.f, 7, 50, 25);
//        self.textField.hidden = YES;
//        self.deviceLabel.hidden = YES;
//        [self.textField removeFromSuperview];
//        
//    }else{
        self.sexImgView.hidden = YES;

        if ((self.sectionIndex == 0 && self.rowIndex == 1) || (self.sectionIndex == 1 && self.rowIndex== 2))
        {
            self.deviceLabel.hidden = NO;
            self.textField.hidden = YES;
            //[self.textField removeFromSuperview];
            self.deviceLabel.frame = CGRectMake(kMainScreenWidth - 165.f, 7, 150, 30);
            if (self.rowIndex == 3) {
                //self.deviceLabel.text = @"必选";
                //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        }else{
            self.deviceLabel.hidden = YES;
            self.textField.hidden = NO;
            //[self.textField removeFromSuperview];
            self.textField.frame = CGRectMake(kMainScreenWidth - 145.f, 7, 130, 30);
            if (self.sectionIndex <2)
            {
                if (self.sectionIndex == 0 && self.rowIndex == 0)
                {
                    self.textField.placeholder = @"请输入IMEI后八位";
                }else{
                    self.textField.placeholder = @"必填";
                }
                
                [self.textField setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
                [self.textField setValue:Font_14 forKeyPath:@"_placeholderLabel.font"];

            }
            
            if (self.sectionIndex == 2) {
                self.textField.placeholder = @"";
            }
            
        }
    //}
}


- (void)setIsSex:(BOOL)isSex
{
    _isSex = isSex;

    if (_isSex)
    {
        self.sexImgView.image = [UIImage imageNamed:@"icon_sex_woman"];
        
    }else{
        
        self.sexImgView.image = [UIImage imageNamed:@"icon_sex_man"];
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
