//
//  RegisterTableViewCell.h
//  GNETS
//
//  Created by tcnj on 16/2/19.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterTableViewCell : UITableViewCell

@property (nonatomic, assign) BOOL isSex;

@property (nonatomic, strong) UILabel *titleLab;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *deviceLabel;
@property (nonatomic, strong) UIImageView *sexImgView;

@property (nonatomic, assign) NSInteger sectionIndex;
@property (nonatomic, assign) NSInteger rowIndex;

@property (nonatomic,strong)UIView *lineView;

@end
