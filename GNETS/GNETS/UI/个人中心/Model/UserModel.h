//
//  _root_.h
//  电动车秘书
//
//  Created by _author on 16-02-21.
//  Copyright (c) _companyname. All rights reserved.
//

/*
	
*/


#import <Foundation/Foundation.h>
#import "DTApiBaseBean.h"
@class Userdata;


@interface UserModel : NSObject
{
	NSNumber *_code;
	Userdata *_data;
	NSString *_errmsg;
}

@property (nonatomic, copy) NSNumber *code;
@property (nonatomic, retain) Userdata *data;
@property (nonatomic, copy) NSString *errmsg;

-(id)initWithDictionary:(NSDictionary*)dict;
-(NSDictionary*)dictionaryValue;
@end
 