//
//  CarDataModel.m
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "CarDataModel.h"

@implementation CarDataModel

- (id)initWithaDic:(NSDictionary *)aDic{
    if (self == [super init]) {
        self.code = [NSString stringWithFormat:@"%@",aDic[@"code"]];
        self.code = [NSString stringWithFormat:@"%@",aDic[@"errmsg"]];
        self.carId = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carId"]];
        self.carPrice = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carPrice"]];
        self.carBrand = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carBrand"]];
        self.carPlate = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carPlate"]];
        self.carColor = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carColor"]];

        self.carModel = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carModel"]];
        if([aDic[@"data"][@"carDate"] isEqualToString:@"(null)"] || !aDic[@"data"][@"carDate"]){
            self.carDate = @"";
        }else{
            self.carDate = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carDate"]];
        }
        self.carType = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carType"]];
        self.carPic = [NSString stringWithFormat:@"%@",aDic[@"data"][@"carPic"]];
        self.frameNum = [NSString stringWithFormat:@"%@",aDic[@"data"][@"frameNum"]];
        self.motorNum = [NSString stringWithFormat:@"%@",aDic[@"data"][@"motorNum"]];
        NSLog(@"sad==%@",aDic[@"data"][@"carDate"]);
    }
    return self;
}

@end
