//
//  data.m
//  电动车秘书
//
//  Created by _author on 16-02-21.
//  Copyright (c) _companyname. All rights reserved.
//  

/*
	
*/


#import "Userdata.h"
#import "DTApiBaseBean.h"


@implementation Userdata

@synthesize activeDate = _activeDate;
@synthesize activeType = _activeType;
@synthesize address = _address;
@synthesize area = _area;
@synthesize carId = _carId;
@synthesize city = _city;
@synthesize expireDate = _expireDate;
@synthesize idNum = _idNum;
@synthesize imei = _imei;
@synthesize phone = _phone;
@synthesize province = _province;
@synthesize salesman = _salesman;
@synthesize sex = _sex;
@synthesize telNum = _telNum;
@synthesize userName = _userName;
@synthesize userToken = _userToken;
@synthesize insurUpdateTime = _insurUpdateTime;
@synthesize insurNum = _insurNum;
@synthesize workPhone = _workPhone;
@synthesize orgName = _orgName;


-(id)initWithDictionary:(NSDictionary*)dict
{
    if (self = [super init])
    {
		DTAPI_DICT_ASSIGN_STRING(activeDate, @"");
		DTAPI_DICT_ASSIGN_NUMBER(activeType, @"0");
		DTAPI_DICT_ASSIGN_STRING(address, @"");
		DTAPI_DICT_ASSIGN_STRING(area, @"");
		DTAPI_DICT_ASSIGN_NUMBER(carId, @"0");
		DTAPI_DICT_ASSIGN_STRING(city, @"");
		DTAPI_DICT_ASSIGN_STRING(expireDate, @"");
		DTAPI_DICT_ASSIGN_STRING(idNum, @"");
		DTAPI_DICT_ASSIGN_NUMBER(imei, @"0");
		DTAPI_DICT_ASSIGN_STRING(phone, @"");
        DTAPI_DICT_ASSIGN_STRING(workPhone, @"");
        DTAPI_DICT_ASSIGN_STRING(orgName, @"");

		DTAPI_DICT_ASSIGN_STRING(province, @"");
		DTAPI_DICT_ASSIGN_STRING(salesman, @"");
		DTAPI_DICT_ASSIGN_NUMBER(sex, @"0");
		DTAPI_DICT_ASSIGN_STRING(telNum, @"");
		DTAPI_DICT_ASSIGN_STRING(userName, @"");
		DTAPI_DICT_ASSIGN_STRING(userToken, @"");
        DTAPI_DICT_ASSIGN_STRING(insurUpdateTime, @"");
        DTAPI_DICT_ASSIGN_STRING(insurNum, @"");

    }
    
    return self;
}

-(NSDictionary*)dictionaryValue
{
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    
	DTAPI_DICT_EXPORT_BASICTYPE(activeDate);
	DTAPI_DICT_EXPORT_BASICTYPE(activeType);
	DTAPI_DICT_EXPORT_BASICTYPE(address);
	DTAPI_DICT_EXPORT_BASICTYPE(area);
	DTAPI_DICT_EXPORT_BASICTYPE(carId);
	DTAPI_DICT_EXPORT_BASICTYPE(city);
	DTAPI_DICT_EXPORT_BASICTYPE(expireDate);
	DTAPI_DICT_EXPORT_BASICTYPE(idNum);
	DTAPI_DICT_EXPORT_BASICTYPE(imei);
	DTAPI_DICT_EXPORT_BASICTYPE(phone);
	DTAPI_DICT_EXPORT_BASICTYPE(province);
	DTAPI_DICT_EXPORT_BASICTYPE(salesman);
	DTAPI_DICT_EXPORT_BASICTYPE(sex);
	DTAPI_DICT_EXPORT_BASICTYPE(telNum);
	DTAPI_DICT_EXPORT_BASICTYPE(userName);
	DTAPI_DICT_EXPORT_BASICTYPE(userToken);
    DTAPI_DICT_EXPORT_BASICTYPE(insurUpdateTime);
    DTAPI_DICT_EXPORT_BASICTYPE(insurNum);
    DTAPI_DICT_EXPORT_BASICTYPE(workPhone);
    DTAPI_DICT_EXPORT_BASICTYPE(orgName);

    return md;
}
@end
