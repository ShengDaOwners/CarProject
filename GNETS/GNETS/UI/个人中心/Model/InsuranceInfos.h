//
//  InsuranceInfo.h
//  GNETS
//
//  Created by fyc on 16/2/29.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InsuranceInfos : NSObject

@property (nonatomic ,retain)NSString *code;
@property (nonatomic ,retain)NSString *errmsg;

@property (nonatomic ,retain)NSString *carId;
@property (nonatomic ,retain)NSString *city;
@property (nonatomic ,retain)NSString *endDate;
@property (nonatomic ,retain)NSString *idNum;
@property (nonatomic ,retain)NSString *insurNum;
@property (nonatomic ,retain)NSString *notify;
@property (nonatomic ,retain)NSString *phone;
@property (nonatomic ,retain)NSString *province;
@property (nonatomic ,retain)NSString *userName;
@property (nonatomic ,retain)NSString *startDate;

@property (nonatomic ,retain)NSString *startDateStr;
@property (nonatomic ,retain)NSString *endDateStr;


- (id)initWithaDic:(NSDictionary *)aDic;
@end
