//
//  City.h
//  GNETS
//
//  Created by fyc on 16/3/1.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (nonatomic, assign)int _id;
@property (nonatomic, assign)int cid;
@property (nonatomic, retain)NSString *name;
@property (nonatomic, assign)int province_id;



@end
