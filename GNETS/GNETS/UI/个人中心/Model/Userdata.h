//
//  data.h
//  电动车秘书
//
//  Created by _author on 16-02-21.
//  Copyright (c) _companyname. All rights reserved.
//

/*
	
*/


#import <Foundation/Foundation.h>
#import "DTApiBaseBean.h"


@interface Userdata : NSObject
{
	NSString *_activeDate;
	NSNumber *_activeType;
	NSString *_address;
	NSString *_area;
	NSNumber *_carId;
	NSString *_city;
	NSString *_expireDate;
	NSString *_idNum;
	NSNumber *_imei;
	NSString *_phone;
	NSString *_province;
	NSString *_salesman;
	NSNumber *_sex;
	NSString *_telNum;
	NSString *_userName;
	NSString *_userToken;
    NSString *_insurUpdateTime;
    NSString *_insurNum;
    NSString *_workPhone;
    NSString *_orgName;


}

//登录接口
@property (nonatomic, copy) NSString *activeDate;
@property (nonatomic, copy) NSNumber *activeType;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSNumber *carId;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *expireDate;
@property (nonatomic, copy) NSString *idNum;
@property (nonatomic, copy) NSNumber *imei;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *workPhone;

@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *salesman;
@property (nonatomic, copy) NSNumber *sex;
@property (nonatomic, copy) NSString *telNum;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userToken;
@property (nonatomic, copy) NSString *orgName;

//用户信息接口
@property (nonatomic, copy) NSString *insurUpdateTime;
@property (nonatomic, copy) NSString *insurNum;

-(id)initWithDictionary:(NSDictionary*)dict;
-(NSDictionary*)dictionaryValue;
@end
 