//
//  CarDataModel.h
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarDataModel : NSObject

@property (nonatomic,retain)NSString *code;
@property (nonatomic,retain)NSString *errmsg;

/*data内层*/
@property (nonatomic,retain)NSString *carBrand;
@property (nonatomic,retain)NSString *carDate;
@property (nonatomic,retain)NSString *carId;
@property (nonatomic,retain)NSString *carModel;
@property (nonatomic,retain)NSString *carPic;
@property (nonatomic,retain)NSString *carPrice;
@property (nonatomic,retain)NSString *carType;
@property (nonatomic,retain)NSString *frameNum;
@property (nonatomic,retain)NSString *motorNum;

@property (nonatomic,retain)NSString *carPlate;
@property (nonatomic,retain)NSString *carColor;


- (id)initWithaDic:(NSDictionary *)aDic;

@end
