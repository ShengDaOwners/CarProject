//
//  LoginViewController.h
//  GNETS
//
//  Created by cqz on 16/2/17.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface LoginViewController : BaseViewController

@property (strong, nonatomic)  UITextField *phoneText;

@property (strong, nonatomic)  UITextField *passwordText;
@property (strong, nonatomic)  UIButton *loginBtn;
@property (strong, nonatomic)  UIButton *forgetBtn;
@property (strong, nonatomic)  UILabel *buildLabel;

@property (weak, nonatomic) IBOutlet UIButton *installBtn;
@property (weak, nonatomic) IBOutlet UIButton *clickBtn;
- (IBAction)clickRegisterEvent:(id)sender;
- (IBAction)ToMakeAnAppointmentEvent:(id)sender;

@end
