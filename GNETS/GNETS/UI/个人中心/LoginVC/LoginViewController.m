//
//  LoginViewController.m
//  GNETS
//
//  Created by cqz on 16/2/17.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "LoginViewController.h"
#import "NSString+MD5.h"
#import "RegisterViewController.h"
#import "GPSViewController.h"
#import "WarningMsgVC.h"
#import "StatisticalVC.h"
#import "UserModel.h"
#import "Userdata.h"
#import "InstallViewController.h"
#import "JPUSHService.h"

#import "GNETSMenuVC.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import <QuartzCore/QuartzCore.h>
#import "GNETSNaigationController.h"

@interface LoginViewController ()<UITextFieldDelegate>
{
    UITapGestureRecognizer * _tapGesture;

}
@property (nonatomic, strong) UIWebView *callPhoneWebView;
@property (nonatomic, strong) MMDrawerController * drawerController;
@property (nonatomic, strong) UIImageView *phoneImg;
@property (nonatomic, strong) UIImageView *codeImg;

@end

@implementation LoginViewController
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];//移除观察者
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.phoneText];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:self.passwordText];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setupNaviBarWithTitle:@"登录"];

    [self initView];
    
}
- (void)initView
{
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    _tapGesture.cancelsTouchesInView = NO;//关键代码
    [self.view addGestureRecognizer:_tapGesture];
    self.buildLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 205, kMainScreenWidth - 160, 20)];
    self.buildLabel.font = Font_18;
    self.buildLabel.backgroundColor = [UIColor clearColor];
    self.buildLabel.textAlignment = NSTextAlignmentCenter;
    [self.buildLabel setTextColor:[UIColor colorWithHexString:@"#0096D7"]];
    [self.view addSubview:self.buildLabel];
    
    //1
    _phoneImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 255, 30, 30)];
    _phoneImg.image = [UIImage imageNamed:@"accountNo"];
    [self.view addSubview:_phoneImg];
    self.phoneText = [[UITextField alloc] initWithFrame:CGRectMake(50, 255, kMainScreenWidth - 75, 30)];
    self.phoneText.borderStyle = UITextBorderStyleNone;
    self.phoneText.font = Font_16;
//    self.phoneText.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneText.placeholder = @"设备编号/车牌号码";
    self.phoneText.textColor = [UIColor colorWithHexString:@"#0096D7"];
    [self.phoneText setValue:[UIColor colorWithHexString:@"#CCCCCC"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.phoneText setValue:Font_16 forKeyPath:@"_placeholderLabel.font"];
    [self.view addSubview:self.phoneText];
    
    //2
    _codeImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 305, 30, 30)];
    _codeImg.image = [UIImage imageNamed:@"passwordNo"];
    [self.view addSubview:_codeImg];
    self.passwordText = [[UITextField alloc] initWithFrame:CGRectMake(50, 305, kMainScreenWidth - 75, 30)];
    self.passwordText.borderStyle = UITextBorderStyleNone;
    self.passwordText.placeholder = @"密码";
    [self.passwordText setValue:[UIColor colorWithHexString:@"#CCCCCC"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordText setValue:Font_16 forKeyPath:@"_placeholderLabel.font"];
    self.passwordText.font = Font_16;
    self.passwordText.textColor = [UIColor colorWithHexString:@"#0096D7"];
    [self.view addSubview:self.passwordText];
    
    self.buildLabel.text = @"汽车秘书";//[NSString stringWithFormat:@"电动车秘书V%@",[ConFunc getBuildVersion]];
    self.phoneText.delegate = self;
    self.phoneText.returnKeyType = UIReturnKeyDone;
    self.passwordText.delegate = self;
    self.passwordText.returnKeyType = UIReturnKeyDone;
    self.passwordText.secureTextEntry = YES;
    self.phoneText.tag = 2001;
    self.passwordText.tag = 2002;
    
    self.loginBtn.enabled = NO;
    
   
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(15, Orgin_y(self.phoneText)+5, kMainScreenWidth - 30, .6)];
    lineView1.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [self.view addSubview:lineView1];
    
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(15, Orgin_y(self.passwordText)+5, kMainScreenWidth - 30, .6)];
    lineView2.backgroundColor = [UIColor colorWithHexString:@"#E5E5E5"];
    [self.view addSubview:lineView2];
    
    //btn
    self.loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginBtn.frame = CGRectMake(20, 360, kMainScreenWidth-30.f, 40);
    [self.loginBtn setTitle:@"登录" forState:0];
    [self.loginBtn addTarget:self action:@selector(goTologinEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.loginBtn.titleLabel.font = [UIFont systemFontOfSize:18.f];
    self.loginBtn.enabled = NO;
    [self.view addSubview:self.loginBtn];
    
    self.forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.forgetBtn.frame = CGRectMake(kMainScreenWidth - 105 , 405,90, 25);
    [self.forgetBtn setTitle:@"忘记密码？" forState:0];
    [self.forgetBtn addTarget:self action:@selector(forgetPasswordEvent:) forControlEvents:UIControlEventTouchUpInside];
    self.forgetBtn.titleLabel.font = Font_16;
    self.forgetBtn.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.forgetBtn];
    
    [self.forgetBtn setTitleColor:[UIColor colorWithHexString:@"#0096D7"] forState:0];
    self.loginBtn.backgroundColor = [UIColor colorWithHexString:@"#0096D7"];
    [self.loginBtn setTitleColor:[UIColor colorWithHexString:@"#66CCF5"] forState:0];
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.layer.cornerRadius = 5.f;
    [self.clickBtn setTitleColor:[UIColor colorWithHexString:@"#0096D7"] forState:0];
    [self.installBtn setTitleColor:[UIColor colorWithHexString:@"#0096D7"] forState:0];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)textFieldChanged:(NSNotification*)noti{
    
    UITextField *textField = (UITextField *)noti.object;
    
    BOOL flag=[NSString isContainsTwoEmoji:textField.text];
    if (flag){
        SHOW_ALERT(@"不能输入表情!");
        textField.text = [NSString disable_emoji:textField.text];
    }
    
    if (self.phoneText.text.length >0)
    {
         _phoneImg.image = [UIImage imageNamed:@"account"];
    }else{
         _phoneImg.image = [UIImage imageNamed:@"accountNo"];
    }
    
    if (self.passwordText.text.length >0)
    {
        _codeImg.image = [UIImage imageNamed:@"password"];
        
    }else{
        _codeImg.image = [UIImage imageNamed:@"passwordNo"];
    }
    
    
    if (self.passwordText.text.length >0 && self.phoneText.text.length >0)
    {
//        self.loginBtn.backgroundColor = [UIColor colorWithHexString:@"#0096D7"];
        [self.loginBtn setTitleColor:[UIColor whiteColor] forState:0];
        self.loginBtn.enabled = YES;
    }else
    {
//        self.loginBtn.backgroundColor = [UIColor colorWithHexString:@"#FB7D30"];
        [self.loginBtn setTitleColor:[UIColor colorWithHexString:@"#66CCF5"] forState:0];
        self.loginBtn.enabled = NO;
    }
    
//    if (textField.tag ==2001){
//        if (textField.text.length >20){
//            SHOW_ALERT(@"亲，您输入的手机号过长");
//            textField.text = [textField.text substringToIndex:12];
//        };
//    }
//    
//    if (textField.tag == 2002) {
//        if (textField.text.length >20){
//            SHOW_ALERT(@"亲，您输入的密码过长");
//            textField.text = [textField.text substringToIndex:12];
//        };
//    }
    
}

#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyBoard];
    return true;
}
#pragma mark -UIButtonEvent
- (void)dismissKeyBoard{
    [self.view endEditing:YES];
}
- (void)goTologinEvent:(id)sender {
    
    [SVProgressHUD showWithStatus:@"正在登录..." maskType:SVProgressHUDMaskTypeClear];
    
    UIDevice *device = [UIDevice currentDevice];
    NSString *deviceUDID = [NSString stringWithFormat:@"%@",device.identifierForVendor];
    DLog(@"输出设备的id---%@",deviceUDID);
    NSArray *array = [deviceUDID componentsSeparatedByString:@">"];
    NSString *udidStr = array[1];
    DLog(@"设备标识符:%@",udidStr);
    NSString *tempStr = [udidStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *aliasString = [ConFunc trimStringUUID:(NSMutableString *)tempStr];

    
    NSString *loginUrl = [[NSString stringWithFormat:@"%@%@?loginName=%@&password=%@&system=%@&clientId=%@",kProjectBaseUrl,LOGINURL,self.phoneText.text,[self.passwordText.text md5],[NSString stringWithFormat:@"iOS%@",device.systemVersion],aliasString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    WEAKSELF;
    [GNETS_NetWorkManger GetJSONWithUrl:loginUrl isNeedHead:NO success:^(NSDictionary *jsonDic) {
        
        [PublicFunction showErrorMsg:jsonDic];
        
        
        UserModel *model = [[UserModel alloc] initWithDictionary:jsonDic];
        
        if ([model.code intValue] !=1) {
            [SVProgressHUD showErrorWithStatus:model.errmsg];
            return;
        }
        [JPUSHService setTags:nil alias:aliasString fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias){
            NSLog(@"rescode: %d, \ntags: %@, \nalias: %@\n", iResCode, iTags, iAlias);
        }];

        
        [SVProgressHUD showSuccessWithStatus:@"登录成功"];
        
        [USER_D setObject:self.phoneText.text forKey:@"user_phone"];
        [USER_D setObject:self.passwordText.text forKey:@"user_password"];
        [USER_D synchronize];
        
        [PublicFunction ShareInstance].userToken = model.data.userToken;
        [PublicFunction ShareInstance].m_user = model;

        //存到本地
        [[PublicFunction ShareInstance]loginSuccessWithAccount:jsonDic];
        [weakSelf layOutTheApp];

        
    } fail:^{
        [SVProgressHUD showErrorWithStatus:@"登录失败,请您检查网络！"];
    }];
}
-(void)tagsAliasCallback:(int)iResCode
                    tags:(NSSet*)tags
                   alias:(NSString*)alias
{
    NSLog(@"rescode: %d, \ntags: %@, \nalias: %@\n", iResCode, tags , alias);
}


- (void)forgetPasswordEvent:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"0531-67805000" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"呼叫", nil];
    alert.tag = 1001;
    [alert show];
}

- (IBAction)clickRegisterEvent:(id)sender {
    RegisterViewController *registerVC = [RegisterViewController new];
    [self.navigationController  pushViewController:registerVC animated:YES];
}

- (IBAction)ToMakeAnAppointmentEvent:(id)sender {
    
    InstallViewController *vc = [InstallViewController new];
    [self.navigationController pushViewController:vc animated:YES];
    
}
#pragma mark -UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( alertView.tag == 1001 && buttonIndex == 1)
    {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",alertView.message]]];
        [self.callPhoneWebView loadRequest:request];
    }
}
#pragma mark -打电话
- (UIWebView *)callPhoneWebView {
    if (!_callPhoneWebView) {
        _callPhoneWebView = [[UIWebView alloc] initWithFrame:CGRectZero];
    }
    return _callPhoneWebView;
}

#pragma mark -布局tabbar
- (void)layOutTheApp
{
    UITabBarController *tabbar = [[UITabBarController alloc] init];
    //设定Tabbar的点击后的颜色 #ffa055
    [[UITabBar appearance] setTintColor:[UIColor colorWithHexString:@"#0096D7"]];
    // [[UITabBar appearance] setBackgroundColor:[UIColor colorWithHexString:@"#373737"]];
    [[UITabBar appearance] setBackgroundImage:[ConFunc createImageWithColor:RGBACOLOR(252, 252, 252, 1)
                                                                       size:CGSizeMake(kMainScreenWidth,kTabBarHeight)]];//设置背景，修改颜色是没有用的
    
    //设定Tabbar的颜色
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    UINavigationController *gpsVC = [self newNavigationControllerForClass:[GPSViewController class]
                                                                    title:@"卫星定位"
                                                                itemImage:@"location_ico"
                                                            selectedImage:@"location_ico_hover"];
    UINavigationController *statisticalvc = [self newNavigationControllerForClass:[StatisticalVC class]
                                                                            title:@"每日统计"
                                                                        itemImage:@"char_ico"
                                                                    selectedImage:@"chart_ico_hover"];
    UINavigationController *warnVC = [self newNavigationControllerForClass:[WarningMsgVC class]
                                                                     title:@"报警消息"
                                                                 itemImage:@"alarm_ico"
                                                             selectedImage:@"alarm_ico_hover"];

    tabbar.viewControllers = @[gpsVC,statisticalvc,warnVC];
    
    GNETSNaigationController *navigationController = [[GNETSNaigationController alloc] initWithRootViewController:tabbar];
    GNETSMenuVC *menuController = [GNETSMenuVC new];
    
    UINavigationController * leftSideNavController = [[GNETSNaigationController alloc] initWithRootViewController:menuController];
    [leftSideNavController setRestorationIdentifier:@"MMExampleLeftNavigationControllerRestorationKey"];
    
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:navigationController
                             leftDrawerViewController:leftSideNavController
                             rightDrawerViewController:nil];
    [self.drawerController setShowsShadow:YES];
    [self.drawerController setMaximumLeftDrawerWidth:leftWidth];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    UIWindow *window = [ConFunc getWindow];
    window.rootViewController = self.drawerController;
}
- (GNETSNaigationController *)newNavigationControllerForClass:(Class)controllerClass
                                                        title:(NSString *)title
                                                    itemImage:(NSString *)itemImage
                                                selectedImage:(NSString *)selectedImage
{
    UIViewController *viewController = [[controllerClass alloc] init];
    GNETSNaigationController *theNavigationController = [[GNETSNaigationController alloc]
                                                         initWithRootViewController:viewController];
    theNavigationController.tabBarItem.title = title;
    theNavigationController.tabBarItem.image = [UIImage imageNamed:itemImage];
    theNavigationController.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    theNavigationController.navigationBarHidden = YES;
    return theNavigationController;
}


@end
