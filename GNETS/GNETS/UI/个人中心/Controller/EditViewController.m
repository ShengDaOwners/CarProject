//
//  EditViewController.m
//  GNETS
//
//  Created by cqz on 16/4/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "EditViewController.h"

@interface EditViewController ()<UITextFieldDelegate>
{
    UITextField *_editTextF;
    NSString *_parameterStr;
}

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [self initUI];
}
- (void)initUI
{
    [self setupNaviBarWithTitle:_titleStr];
    [self setupNaviBarWithBtn:NaviLeftBtn title:nil img:@"back_icon"];
    [self setupNaviBarWithBtn:NaviRightBtn title:@"保存" img:nil];
    self.rightBtn.titleLabel.font = Font_16;
    
    _parameterStr = @"";
    if ([_titleStr isEqualToString:@"车辆品牌"]) {
        _parameterStr = @"carBrand";
        
    }else if ([_titleStr isEqualToString:@"车辆型号"]) {
        _parameterStr = @"carModel";

    }if ([_titleStr isEqualToString:@"电机编号"]) {
        _parameterStr = @"motorNum";

    }if ([_titleStr isEqualToString:@"车架编号"]) {
        _parameterStr = @"frameNum";

    }if ([_titleStr isEqualToString:@"车辆颜色"]) {
        _parameterStr = @"carColor";

    }if ([_titleStr isEqualToString:@"购买日期"]) {
        _parameterStr = @"carDate";

    }if ([_titleStr isEqualToString:@"购买价格"]) {
        _parameterStr = @"carPrice";

    }if ([_titleStr isEqualToString:@""]) {
        _parameterStr = @"carPic";
    }
    
    

    UIView *searchView = [[UIView alloc] initWithFrame:CGRectMake(0, 74, kMainScreenWidth, 40)];
    searchView.layer.masksToBounds = YES;
    searchView.layer.borderWidth = .6f;
    searchView.backgroundColor = [UIColor whiteColor];
    searchView.layer.borderColor = [UIColor colorWithHexString:@"#d4d4d4"].CGColor;
    [self.view addSubview:searchView];

    
    _editTextF =[[UITextField alloc] initWithFrame:CGRectMake(10.f, 5, searchView.frame.size.width - 20.f, 30)];
    _editTextF.delegate = self;
    _editTextF.text = _contentStr;
    _editTextF.font = Font_14;
    
    if ([_parameterStr isEqualToString:@"carPrice"]){
        _editTextF.keyboardType = UIKeyboardTypeNumberPad;
    }
    _editTextF.borderStyle = UITextBorderStyleNone;
    _editTextF.returnKeyType = UIReturnKeyDone; //设置按键类型
    [searchView addSubview:_editTextF];
    [_editTextF becomeFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textFieldChanged:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:_editTextF];
    
}
#pragma mark-UIButtonEvent
- (void)rightBtnAction
{
    if (_editTextF.text.length == 0) {
        [JKPromptView showWithImageName:nil message:@"请您填写修改内容！"];
        return;
    }
    
    NSString *postModify = _editTextF.text;
    
    NSDictionary *dict  = [NSDictionary dictionaryWithObjectsAndKeys:[[PublicFunction ShareInstance] getAccount].data.carId,@"carId" ,postModify,_parameterStr,nil];
    WEAKSELF;
    [GNETS_NetWorkManger PostJSONWithUrl:[NSString stringWithFormat:@"%@%@",kProjectBaseUrl,UpdateCar] parameters:dict isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        NSString *str = [NSString stringWithFormat:@"%@",jsonDic[@"code"]];
        if ([str isEqualToString:@"1"]) {
            [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
            weakSelf.editBlock(_editTextF.text);
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    } fail:^{
        
    }];
    
    
}
#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self dismissKeyBoard];
    return true;
}
- (void)textFieldChanged:(NSNotification*)noti{
    
    UITextField *textField = (UITextField *)noti.object;
    
    BOOL flag=[NSString isContainsTwoEmoji:textField.text];
    if (flag){
        textField.text = [NSString disable_emoji:textField.text];
    }
}

#pragma mark - 放下键盘
- (void)dismissKeyBoard{
    [self.view endEditing:YES];
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
