//
//  AboutVC.m
//  GNETS
//
//  Created by fyc on 16/2/29.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "AboutVC.h"

@interface AboutVC ()

@end

@implementation AboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupNaviBarWithTitle:@"关于"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:nil img:@"wpp_readall_top_down_normal"];
    
    self.detailTitleLa.adjustsFontSizeToFitWidth = YES;
    
    self.versionLa.text = [NSString stringWithFormat:@"For iOS V%@",[ConFunc getBuildVersion]];
    self.versionLa.adjustsFontSizeToFitWidth = YES;
    self.companys.adjustsFontSizeToFitWidth = YES;
    self.creatLa.adjustsFontSizeToFitWidth = YES;
    
//    self.versionLa.textColor = [UIColor grayColor];
//    NSAttributedString *version = [[NSAttributedString alloc]initWithString:self.versionLa.text attributes:@{NSFontAttributeName:[UIFont fontWithName:@"CourierNewPSMT" size:15.0]}];
//    self.versionLa.attributedText = version;
//    
//    NSAttributedString *company = [[NSAttributedString alloc]initWithString:@"你号" attributes:@{NSFontAttributeName:[UIFont fontWithName:@"CourierNewPSMT" size:13.0]}];
//    self.companys.attributedText = company;
//    
//    NSAttributedString *creat = [[NSAttributedString alloc]initWithString:self.creatLa.text attributes:@{NSFontAttributeName:[UIFont fontWithName:@"CourierNewPSMT" size:14.0]}];
//    self.creatLa.attributedText = creat;
    
    
    // Do any additional setup after loading the view from its nib.
}
- (void)leftBtnAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
