//
//  EditViewController.h
//  GNETS
//
//  Created by cqz on 16/4/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface EditViewController : BaseViewController

@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, copy) NSString *contentStr;
@property (nonatomic, copy) ZDStringBlock editBlock;
@end
