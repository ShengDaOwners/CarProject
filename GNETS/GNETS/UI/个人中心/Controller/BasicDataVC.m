//
//  BasicDataVC.m
//  GNETS
//
//  Created by fyc on 16/2/23.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "BasicDataVC.h"
#import "BasicDataCell.h"

@interface BasicDataVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSString *_orgName;
}

@property (nonatomic,retain)NSArray *data;
@property (nonatomic,retain)UITableView *selfDataTable;

@end

@implementation BasicDataVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupNaviBarWithTitle:@"基本资料"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:nil img:@"wpp_readall_top_down_normal"];
    
    self.data = [NSMutableArray array];
    
    self.selfDataTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight - 64) style:UITableViewStyleGrouped];
    self.selfDataTable.delegate = self;
    self.selfDataTable.dataSource = self;
    self.selfDataTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.selfDataTable];
    
    [self.selfDataTable registerNib:[UINib nibWithNibName:@"BasicDataCell" bundle:nil] forCellReuseIdentifier:@"BasicDataCell"];
    
    [self getUserBasicData];
    
}
- (void)getUserBasicData{
    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,INFOURL,[[PublicFunction ShareInstance]getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
        //更新用户数据
        [[PublicFunction ShareInstance]loginSuccessWithAccount:jsonDic];
        
        UserModel *user = [[UserModel alloc]initWithDictionary:jsonDic];
        
        NSString *sex = user.data.sex.intValue == 0?@"男":@"女";
        
        if (user.data.orgName.length ==0)
        {
            self.data = @[@[@{@"title":@"设备编号",@"detail":[NSString stringWithFormat:@"%@",user.data.carId]}],@[@{@"title":@"IMEI",@"detail":[NSString stringWithFormat:@"%@",user.data.imei]},@{@"title":@"数据卡号",@"detail":user.data.telNum},@{@"title":@"开通时间",@"detail":user.data.activeDate}],@[@{@"title":@"姓名",@"detail":user.data.userName},@{@"title":@"性别",@"detail":sex},@{@"title":@"联系电话",@"detail":user.data.phone},@{@"title":@"工作电话",@"detail":user.data.workPhone},@{@"title":@"所在地区",@"detail":[NSString stringWithFormat:@"%@%@%@",user.data.province,user.data.city,user.data.area]},@{@"title":@"详细地址",@"detail":user.data.address}]];

            
        }else{
            self.data = @[@[@{@"title":@"设备编号",@"detail":[NSString stringWithFormat:@"%@",user.data.carId]}],@[@{@"title":@"IMEI",@"detail":[NSString stringWithFormat:@"%@",user.data.imei]},@{@"title":@"数据卡号",@"detail":user.data.telNum},@{@"title":@"开通时间",@"detail":user.data.activeDate}],@[@{@"title":@"姓名",@"detail":user.data.userName},@{@"title":@"性别",@"detail":sex},@{@"title":@"联系电话",@"detail":user.data.phone},@{@"title":@"工作电话",@"detail":user.data.workPhone},@{@"title":@"所在地区",@"detail":[NSString stringWithFormat:@"%@%@%@",user.data.province,user.data.city,user.data.area]},@{@"title":@"详细地址",@"detail":user.data.address}],@[@{@"title":@"所属单位",@"detail":[NSString stringWithFormat:@"%@",user.data.orgName]}]];
        }
        
        [self.selfDataTable reloadData];
        
    } fail:^{
        
    }];
}
#pragma mark - table
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 3;
    }else if (section == 2){
        return 6;
    }
    return 1;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.data count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    BasicDataCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BasicDataCell"];
    
    cell.titleLa.text = self.data[indexPath.section][indexPath.row][@"title"];
    cell.detailTitleLa.text = self.data[indexPath.section][indexPath.row][@"detail"];
    
    if ([cell.titleLa.text isEqualToString:@"设备编号"]) {
        cell.lineView.hidden = YES;
    }else if ([cell.titleLa.text isEqualToString:@"到期时间"]){
        cell.lineView.hidden = YES;
    }else if ([cell.titleLa.text isEqualToString:@"详细地址"]){
        cell.lineView.hidden = YES;
    }else if ([cell.titleLa.text isEqualToString:@"所属单位"]){
        cell.lineView.hidden = YES;
    }else{
        cell.lineView.hidden = NO;
    }
 
    return cell;
}
- (void)leftBtnAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
