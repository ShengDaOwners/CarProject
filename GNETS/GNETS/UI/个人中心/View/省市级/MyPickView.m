//
//  MyPickView.m
//  ZhouDao
//
//  Created by cqz on 16/3/5.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "MyPickView.h"
//#import "UIImageView+LBBlurredImage.h"
//#import "QHCommonUtil.h"
#import "Country.h"
#import "Province.h"
#import "City.h"
#import "Districtlist.h"


#define VIEWWITH   [UIScreen mainScreen].bounds.size.width/3.f

#define kScreen_Height      ([UIScreen mainScreen].bounds.size.height)
#define kScreen_Width       ([UIScreen mainScreen].bounds.size.width)
#define kScreen_Frame       (CGRectMake(0, 0 ,kScreen_Width,kScreen_Height))
@interface MyPickView() <UIPickerViewDataSource, UIPickerViewDelegate>
{
    UIImageView *_mainBackgroundIV;
}
@property (strong, nonatomic) UIView *maskView;
@property (strong, nonatomic)  UIPickerView *myPicker;
@property (strong, nonatomic)  UIView *pickerBgView;

//data
@property (nonatomic, strong) NSString *provinceStr;
@property (nonatomic, strong) NSString *cityStr;
@property (nonatomic, strong) NSString *districtStr;

@property (nonatomic, strong) NSArray *ProvinceArray;
@property (nonatomic, strong) NSArray *CityArray;
@property (nonatomic, strong) NSArray *districtArray;

@end

@implementation MyPickView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self getPickerData];
        [self initView];
        //[self configureViewBlurWith:self.frame.size.width scale:0.4];


    }
    return self;
}

#pragma mark - init view
- (void)initView {
    
    float width = self.frame.size.width;
    float height = self.frame.size.height;
    
    self.maskView = [[UIView alloc] initWithFrame:kScreen_Frame];
    self.maskView.backgroundColor = [UIColor blackColor];
    self.maskView.alpha = 0.3;
    [self.maskView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideMyPicker)]];
    [self addSubview:self.maskView];
    
    self.pickerBgView = [[UIView alloc] initWithFrame:CGRectMake(0, height, width, 255)];
    self.pickerBgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.pickerBgView];
    
    self.myPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 39, width, 216)];
    self.myPicker.showsSelectionIndicator=YES;//显示选中框
    self.myPicker.delegate = self;
    self.myPicker.dataSource = self;
    [self.pickerBgView addSubview:self.myPicker];
    
    UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainScreenWidth, 40)];
    toolView.backgroundColor = RGBACOLOR(239, 239, 239, 1);
    [self.pickerBgView addSubview:toolView];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitleColor:[UIColor redColor] forState:0];
    [cancelBtn setTitle:@"取消" forState:0];
    [cancelBtn addTarget:self action:@selector(cancelEvent:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.titleLabel.font = Font_14;
    cancelBtn.frame = CGRectMake(0, 7, 80, 24);
    cancelBtn.backgroundColor = [UIColor clearColor];
    [toolView addSubview:cancelBtn];
    
    UIButton *ensureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [ensureBtn setTitleColor:[UIColor redColor] forState:0];
    [ensureBtn setTitle:@"确定" forState:0];
    [ensureBtn addTarget:self action:@selector(ensureEvent:) forControlEvents:UIControlEventTouchUpInside];
    ensureBtn.titleLabel.font = Font_14;
    ensureBtn.backgroundColor = [UIColor clearColor];
    ensureBtn.frame = CGRectMake(width-80, 8, 80, 24);
    [toolView addSubview:ensureBtn];
    
    
    //获取默认地区 选择到响应的pickview
    for (NSUInteger i=0; i<self.ProvinceArray.count; i++)
    {
        Province *province = self.ProvinceArray[i];
        if ([province.name isEqualToString:[PublicFunction ShareInstance].m_user.data.province])
        {
            self.provinceStr = province.name;
            self.CityArray = [GNETSSqlitManager getCityeListWithProvinceId:[NSString stringWithFormat:@"%d",province.pid]];
            for (NSUInteger j = 0; j < self.CityArray.count; j ++) {
                City *city = self.CityArray[j];
                NSLog(@"city=%@",city.name);
                if ([city.name isEqualToString:[PublicFunction ShareInstance].m_user.data.city]) {
                    [self.myPicker selectRow:i inComponent:j animated:NO];
                    [self.myPicker reloadComponent:1];
                    
                }
            }
        }
    }

    
    WEAKSELF;
    [UIView animateWithDuration:.35f animations:^{
        weakSelf.pickerBgView.frame = CGRectMake(0, height-255, width, 255);
    }];
}
#pragma mark - get data
- (void)getPickerData {
    
    //获取所有的国家
    NSMutableArray *countryList = [GNETSSqlitManager getCountryList];
    for (Country *coutry in countryList) {
        if ([coutry.name isEqualToString:@"中国"]) {
            self.ProvinceArray = [GNETSSqlitManager getProvinceListWithContryId:[NSString stringWithFormat:@"%d",coutry.cid]];
        }
    }
    


    
}
#pragma mark - UIPicker Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component==0)
    {
        return self.ProvinceArray.count;
    }
    else if(component==1)
    {
        if ([pickerView selectedRowInComponent:0]==-1)
        {
            return 0;
        }
        else
        {
            Province *province = self.ProvinceArray[[_myPicker selectedRowInComponent:0]];
            self.CityArray = [GNETSSqlitManager getCityeListWithProvinceId:[NSString stringWithFormat:@"%d",province.pid]];
            
            return [self.CityArray count];
        }
    }
    else
    {
        if ([pickerView selectedRowInComponent:1]==-1)
        {
            return 0;
        }
        else
        {
            City *city = self.CityArray[[_myPicker selectedRowInComponent:1]];
            self.districtArray = [GNETSSqlitManager getDistrictListWithCityId:[NSString stringWithFormat:@"%d",city.cid]];
            return self.districtArray.count;
        }
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    if (component == 0) {
        return 100;
    } else if (component == 1) {
        return 110;
    } else {
        return 110;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component==0)
    {
        [pickerView reloadComponent:1];
        
        [pickerView reloadComponent:2];
        [self getSelectDistrictName];
    }
    else if(component==1)
    {
        [self getSelectDistrictName];
        [pickerView reloadComponent:2];

    }
    else
    {
        //刷新滑动后页面数据
        [self getSelectDistrictName];
    }

}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view

{
    UILabel *myView = nil;
    
    if (component == 0) {
        
        myView = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, VIEWWITH, 30)];
        
        myView.textAlignment = NSTextAlignmentCenter;
        Province *provice = self.ProvinceArray[row];
        myView.text = provice.name;
        myView.text.length>7?[myView setFont:Font_12]:[myView setFont:Font_14];;
        
        myView.backgroundColor = [UIColor clearColor];
        
    }else if (component == 1){
        
        myView = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, VIEWWITH, 30)];
        
        City *city = self.CityArray[row];
        myView.text = city.name;
        
        myView.textAlignment = NSTextAlignmentCenter;
        myView.text.length>7?[myView setFont:Font_12]:[myView setFont:Font_14];
        myView.backgroundColor = [UIColor clearColor];
        
    }else{
        myView = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, VIEWWITH, 30)];
        
        myView.textAlignment = NSTextAlignmentCenter;
        
        Districtlist *diqu = self.districtArray[row];
        myView.text = diqu.name;
        
        myView.text.length>7?[myView setFont:Font_12]:[myView setFont:Font_14];
        
        myView.backgroundColor = [UIColor clearColor];
        
    }
    
    return myView;
    
}
- (void)hideMyPicker{
    
    float width = self.frame.size.width;
    float height = self.frame.size.height;

    WEAKSELF;
    [UIView animateWithDuration:.35f animations:^{
        weakSelf.pickerBgView.frame = CGRectMake(0, height, width, 255);

    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}
#pragma mark - xib click
- (void)cancelEvent:(id)sender {
    [self hideMyPicker];
}
- (void)ensureEvent:(id)sender {
    
    Province *provice = [self.ProvinceArray objectAtIndex:[self.myPicker selectedRowInComponent:0]];
    NSString *str1 = provice.name;
    City *city = [self.CityArray objectAtIndex:[self.myPicker selectedRowInComponent:1]];
    NSString *str2 = city.name;
    Districtlist *district = [self.districtArray objectAtIndex:[self.myPicker selectedRowInComponent:2]];
    NSString *str3 = district.name;
    
    //NSString *pickString = [NSString stringWithFormat:@"%@:%@:%@",str1,str2,str3];
    
    self.pickBlock(str1,str2,str3);
    
    [self hideMyPicker];
}
#pragma mark -获取省市区 地名
- (void)getSelectDistrictName
{
    Province *province = self.ProvinceArray[[_myPicker selectedRowInComponent:0]];
    self.provinceStr = province.name;
    
}
//- (void)configureViewBlurWith:(float)nValue scale:(float)nScale
//{
//    if(_mainBackgroundIV == nil)
//    {
//        _mainBackgroundIV = [[UIImageView alloc] initWithFrame:self.bounds];
//        _mainBackgroundIV.userInteractionEnabled = YES;
//        
//        UIImage *image = [QHCommonUtil getImageFromView:self];
//        [_mainBackgroundIV setImageToBlur:image
//                               blurRadius:kLBBlurredImageDefaultBlurRadius
//                          completionBlock:^(){}];
//        
//        [self addSubview:_mainBackgroundIV];
//    }
//    [_mainBackgroundIV setAlpha:(nValue/self.frame.size.width) * nScale];
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
