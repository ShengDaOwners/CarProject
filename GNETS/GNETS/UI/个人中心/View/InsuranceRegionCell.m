//
//  InsuranceRegionCell.m
//  GNETS
//
//  Created by fyc on 16/2/18.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "InsuranceRegionCell.h"

@implementation InsuranceRegionCell

- (void)awakeFromNib {
    // Initialization code
    //self.regionLa.adjustsFontSizeToFitWidth = YES;
//
    self.regionLa = [[UILabel alloc] init];
    self.regionLa.font = Font_12;
    self.regionLa.textAlignment = NSTextAlignmentRight;
    self.regionLa.textColor = [UIColor colorWithHexString:@"#666666"];
    self.regionLa.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 124, 7, 100, 28);
    [self.contentView addSubview:self.regionLa];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
