//
//  NavMapViewController.h
//  GNETS 秘书
//
//  Created by tcnj on 15/10/30.
//
//

#import <UIKit/UIKit.h>
#import <AMapNaviKit/MAMapKit.h>
#import <AMapNaviKit/AMapNaviKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>

#import "iflyMSC/IFlySpeechSynthesizer.h"
#import "iflyMSC/IFlySpeechSynthesizerDelegate.h"

@protocol NavMapViewControllerPro <NSObject>

- (void)dismissEvents;

@end

typedef void(^IFBlock)(void);
@interface NavMapViewController : UIViewController<MAMapViewDelegate,AMapSearchDelegate,AMapNaviManagerDelegate,IFlySpeechSynthesizerDelegate>


@property (nonatomic,weak) id<NavMapViewControllerPro>delegate;

@property (nonatomic, strong) AMapNaviPoint *startPoint; //导航时候 起点
@property (nonatomic, strong) AMapNaviPoint *endPoint; //导航时候 目标终点
@property (nonatomic, strong) MAUserLocation *userLocation;  //导航时候 起点 （定位获取，定位失败后手动选择）

@property (nonatomic,copy)IFBlock navBlock;
@property (nonatomic, strong) AMapSearchAPI *search;

@property (nonatomic, strong) AMapNaviManager *naviManager;

@property (nonatomic, strong) IFlySpeechSynthesizer *iFlySpeechSynthesizer;

@end
