//
//  GPSViewController.m
//  GNETS
//
//  Created by tcnj on 16/2/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "GPSViewController.h"
#import <AMapNaviKit/MAMapKit.h>
#import "CustomAnnotationView.h"
#import "LocInfoModel.h"
#import "NavMapViewController.h"
#import "MapTypeView.h"
#import "UIView+Tap.h"
#import "MsgView.h"
#import "SelectTimeAlert.h"
#import "trackModel.h"
#import "CustomMAPointAnnotation.h"
#import "UIViewController+MMDrawerController.h"
#import "MMDrawerController.h"

#define REES_TO_RADIANS(angle) ((angle)/180.0 *M_PI)

@interface GPSViewController ()<MAMapViewDelegate,MapTypeViewPro,NavMapViewControllerPro,CustomAnnotationViewPro,SelectTimeAlertPro>
{
    NSMutableArray *_poiAnnotations;
    BOOL _isTrack;//是否显示的是轨迹
    UITapGestureRecognizer* _singleTap;//地图点击手势
    CLLocationCoordinate2D _center;
}
@property (nonatomic,strong)NSTimer             *myTimer;
@property (nonatomic, strong) LocInfoModel *locModel;//车辆位置信息
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) MapTypeView *mapTypeView;
@property (nonatomic, strong) MsgView *msgView;
@property (nonatomic, strong) NSMutableArray *overlays;
@property (nonatomic, copy) NSString *anoTitle;
@property (nonatomic,strong) SelectTimeAlert *alertViewTime;
@property (nonatomic, strong) id<MAAnnotation>annotation;

@end

@implementation GPSViewController
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.mapView  removeGestureRecognizer:_singleTap];//清除点击手势
    [self dismissEvents];
    [self loadCarLocation];
    if (_myTimer == nil){
        _myTimer = [NSTimer scheduledTimerWithTimeInterval:30.f target:self selector:@selector(reloadMapLocation:) userInfo:nil repeats:YES];
    }
}
- (void)reloadMapLocation:(NSTimer *)theTimer
{
    [self loadCarLocation];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (_myTimer) {
       [_myTimer invalidate];
    }
        
    if (_alertViewTime) {
        [_alertViewTime removeFromSuperview];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    [self initIFlySpeech];//讯飞语音
    self.navigationController.navigationBarHidden = YES;

    [self initMapView];
    
    //检测是围栏车成功

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleLockCarStatus:) name:@"OpenVf" object:nil];
    
}
#pragma -mark 处理是否开启围栏成功
- (void)handleLockCarStatus:(NSNotification *)sender{
    NSString *eventType = [sender.userInfo objectForKey:@"eventType"];
    //8关闭超时 7关闭成功 6开启超时 5开启成功
    if (eventType.intValue == 8) {
//        self.locModel.data.lock = @"1";
        self.locModel.data.isOpenVf = @1;
        [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_open"] forState:0];

    }else if(eventType.intValue == 7){
        self.locModel.data.isOpenVf = @0;
        [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_close"] forState:0];
    }else if (eventType.intValue == 6){
        self.locModel.data.isOpenVf = @0;
        [self.mapView removeOverlays:self.mapView.overlays];
        [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_close"] forState:0];
    }else if (eventType.intValue == 4){
        self.locModel.data.isOpenVf = @1;
        [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_open"] forState:0];
    }
}

- (void)initMapView{
    
    [self setupNaviBarWithTitle:@"卫星定位"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:nil img:@"icon_title_user"];

    _poiAnnotations = [[NSMutableArray alloc] init];

    if (self.mapView == nil) {
        self.mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0,64 , self.view.frame.size.width, self.view.frame.size.height-64)] ;
        self.mapView.delegate = self;
        //self.mapView.showsUserLocation = YES;
        self.mapView.showsCompass = NO;
        self.mapView.showsScale = NO;
        [self.mapView setZoomLevel:16.1 animated:YES];
        [self.view addSubview:self.mapView];
        _locationService = [[AMapLocationManager alloc] init];
        _locationService.delegate = self;
        [_locationService startUpdatingLocation];//开启定位
    }
    
    NSString *trafficStr =[USER_D objectForKey:@"showTraffic"];
    if (trafficStr.length>0) {
        if ([trafficStr isEqualToString:@"YES"]) {
            self.mapView.showTraffic = YES;
            UIButton *trafficBtn = (UIButton *)[self.view viewWithTag:1001];
            [trafficBtn setBackgroundImage:[UIImage imageNamed:@"traffic_open"] forState:0];
        }else{
            self.mapView.showTraffic = NO;
        }
    }
    NSString *typeStr = [USER_D objectForKey:@"MAMapType"];
    if (typeStr.length>0)
    {
        if ([typeStr isEqualToString:@"卫星图"]) {
            self.mapView.mapType = MAMapTypeSatellite;
        }else{
            self.mapView.mapType = MAMapTypeStandard;
        }
    }else{
        self.mapView.mapType = MAMapTypeStandard;
    }
    
    for (NSUInteger i =1; i<8; i++) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:1000+i];
        [self.view bringSubviewToFront:btn];
    }
    NSString *unread = [USER_D objectForKey:@"UNREAD"];
    UITabBarItem *item = self.tabBarController.tabBar.items[1];
    if (unread.length <=0 || [unread isEqualToString:@"0"]) {
        [item setBadgeValue:nil];
    }else{
        [item setBadgeValue:unread];
    }

}
#pragma mark -获取车辆位置信息
- (void)loadCarLocation
{
    NSString *carUrl = [NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,CarLocationInfo,[[PublicFunction ShareInstance]getAccount].data.carId];
    [GNETS_NetWorkManger GetJSONWithUrl:carUrl isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        NSUInteger code = [[jsonDic objectForKey:@"code"] integerValue];

        if (code != 1) {
            return ;
        }
        self.locModel = [[LocInfoModel alloc] initWithDictionary:jsonDic];
        [_poiAnnotations removeAllObjects];
        [self.mapView removeAnnotations:self.mapView.annotations];
        [self.mapView removeOverlays:self.mapView.overlays];
        
        if ([self.locModel.data.isOpenVf integerValue]==0) {
            [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_close"] forState:0];
        }else{
            [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_open"] forState:0];
        }
        
        NSUInteger lat = [self.locModel.data.lat integerValue];
        NSUInteger lon = [self.locModel.data.lon integerValue];
        CLLocationCoordinate2D center = {lat/1000000.0, lon/1000000.0};
        _center = center;
        _isTrack = NO;//显示的不是轨迹

        //导航的终点
        _endPoint = [AMapNaviPoint locationWithLatitude:center.latitude
                                              longitude:center.longitude];
        _anoTitle = [NSString stringWithFormat:@"%@\n%@km/h",self.locModel.data.satelliteTime,self.locModel.data.speed];

        MAPointAnnotation *annotation = [[MAPointAnnotation alloc] init];
        [annotation setCoordinate:CLLocationCoordinate2DMake(center.latitude, center.longitude)];
        [_poiAnnotations addObject:annotation];
        
        if ([self.locModel.data.isOpenVf integerValue] == 1  && [self.locModel.data.speed integerValue] != 0)
        {
            NSUInteger lat1 = [self.locModel.data.vfLat integerValue];
            NSUInteger lon1 = [self.locModel.data.vfLon integerValue];
            CLLocationCoordinate2D center1 = {lat1/1000000.0, lon1/1000000.0};
            MAPointAnnotation *annotation1 = [[MAPointAnnotation alloc] init];
            [annotation1 setCoordinate:CLLocationCoordinate2DMake(center1.latitude, center1.longitude)];
            NSArray *arrAno = [NSArray arrayWithObjects:annotation,annotation1, nil];
            
            [self zoomToMapPoints:_mapView annotations:arrAno];
        }
        [self showPOIAnnotations];
        
    } fail:^{
        
    }];
}
- (void)showPOIAnnotations
{
    [self.mapView addAnnotations:_poiAnnotations];
    
    if (_poiAnnotations.count == 1)
    {
        self.mapView.centerCoordinate = [(MAPointAnnotation *)_poiAnnotations[0] coordinate];
        if ([self.locModel.data.isOpenVf integerValue] == 1){
            [self drawOverlays];
        }
    }
    else
    {
        [self.mapView showAnnotations:_poiAnnotations animated:NO];
    }
}
#pragma mark - MapView Delegate 更新地理位置
- (void)amapLocationManager:(AMapLocationManager *)manager didUpdateLocation:(CLLocation *)location
{
    if (location)
    {
        _userLocation = location;
        [_locationService stopUpdatingLocation];//停止定位
    }
    
    DLog(@"location:{lat:%f; lon:%f; accuracy:%f}", location.coordinate.latitude, location.coordinate.longitude, location.horizontalAccuracy);
}
//- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation{
//    
//    if (updatingLocation)
//    {
//        _userLocation = userLocation;
//        [_locationService stopUpdatingLocation];//停止定位
//    }
//}
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    if (_isTrack == NO) {
        
        if ([annotation isKindOfClass:[MAPointAnnotation class]])
        {
            
            if (self.mapView.annotations.count == 1)
            {
                _annotation = annotation;
                static NSString *pointReuseIndetifier = @"poiIdentifier";
                MAAnnotationView *annotationView = (MAAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndetifier];
                
                if (annotationView == nil)
                {
                    annotationView = [[MAPinAnnotationView alloc] initWithAnnotation:annotation
                                                                      reuseIdentifier:pointReuseIndetifier];
                }
                annotationView.tag = 8888;
                if ([self.locModel.data.isOnline isEqualToString:@"1"]) {
                    annotationView.image = [UIImage imageNamed:@"ebike_online"];
                }else{
                    annotationView.image = [UIImage imageNamed:@"ebike_offline"];
                }
                annotationView.canShowCallout= NO;      //设置气泡可以弹出，默认为NO
                annotationView.draggable = NO;           //设置标注可以拖动，默认为NO
                return annotationView;
                
            }else{
                static NSString *pointReuseIndetifier = @"customReuseIndetifier";
                CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:pointReuseIndetifier];
                if (annotationView == nil)
                {
                    annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation
                                                                      reuseIdentifier:pointReuseIndetifier];
                }
                [annotationView setCalloutText:@""];
                annotationView.delegate = self;
                annotationView.image = [UIImage imageNamed:@"point"];
                annotationView.selected = YES;
                return annotationView;
            }
        }
        
    }else{
        if ([annotation isKindOfClass:[MAPointAnnotation class]])
        {
            static NSString *customReuseIndetifier = @"cReuseIndetifier";
            MAAnnotationView *annotationView = (MAAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:customReuseIndetifier];
            if (annotationView == nil)
            {
                annotationView = [[MAAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:customReuseIndetifier];
                annotationView.canShowCallout = YES;
            }
            annotationView.image = [UIImage imageNamed:@"point"];
            
            if ([annotation.title isEqualToString:@"起点"]) {
                annotationView.image = [UIImage imageNamed:@"marker_start"];
                annotationView.centerOffset = CGPointMake(0, -16);
            }else if ([annotation.title isEqualToString:@"终点"]){
                annotationView.image = [UIImage imageNamed:@"marker_end"];
                annotationView.centerOffset = CGPointMake(0, -16);
            }
            return annotationView;
        }
    }
    return nil;
}
#pragma mark-点击大头针
- (void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view
{
    if (view.tag == 8888) {
        [self.mapView deselectAnnotation: _annotation animated:YES];
        UIButton *btn = (UIButton *)[self.view viewWithTag:1006];
        [self getCarInfo:btn];
    }
}

-(void)addAnnotationWithCooordinate:(CLLocationCoordinate2D)coordinate withTitle:(NSString *)title calloutText:(NSString*)calloutText
{
    MAPointAnnotation *annotation = [[MAPointAnnotation alloc] init];
    [annotation setCoordinate:coordinate];
    [annotation setTitle:title];
    [annotation setSubtitle:calloutText];
    [self.mapView addAnnotation:annotation];
}

#pragma mark -按钮点击事件
- (IBAction)clickButtonEvent:(id)sender
{
    UIButton *button = (UIButton *)sender;
    [self.mapView  removeGestureRecognizer:_singleTap];
    
    NSUInteger index = button.tag;
    
    switch (index) {
        case 1001:
        {
            [self showTrafficEvent:button];
        }
            break;
        case 1002:
        {
            [self changeMapTyleFinder:button];
        }
            break;
        case 1003:
        {//语音寻车
            
        }
            break;
        case 1004:
        {
             _alertViewTime = [[SelectTimeAlert alloc] initWithFrame:self.view.bounds];
            _alertViewTime.delegate = self;
            [_alertViewTime show];
        }
            break;
        case 1005:
        {
            [self.mapView removeAnnotations:self.mapView.annotations];
            if (_isTrack == YES) {
                [self.mapView removeOverlays:self.mapView.overlays];
            }
            _isTrack = NO;
            [self showPOIAnnotations];

            NSString *vfStr = nil;
            if (self.locModel) {
                if ([self.locModel.data.isOpenVf integerValue] == 0) {
                    vfStr = @"您确定要开启电子围栏吗?";

                }else{
                    vfStr = @"您确定要关闭电子围栏吗?";
                }
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:vfStr delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确认", nil];
                alertView.tag = 1573;
                [alertView show];
            }
        }
            break;
        case 1006:{
            [self getCarInfo:button];
        }
            break;
        case 1007:{
            
            [self startCarNav];
        }
            break;
            
        default:
            break;
    }
}
#pragma mark -SelectTimeAlertPro 画轨迹
- (void)ShowTheRoadWithKSDate:(NSMutableArray *)locArrays WithStar:(CLLocationCoordinate2D)qidian withEnd:(CLLocationCoordinate2D)endcll
{
    [_myTimer invalidate];//销毁定时器
    

    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    _isTrack = YES;//显示的是轨迹

    
    if (locArrays.count == 1) {
        CLLocationCoordinate2D point = CLLocationCoordinate2DMake(qidian.latitude, qidian.longitude);
        trackdata *model = (trackdata *)locArrays[0];
        NSString *contentStr = [NSString stringWithFormat:@"%@  速度%@km/h",model.satelliteTimeStr,model.speed];
        [self addAnnotationWithCooordinate:point withTitle:@"起点" calloutText:contentStr];
    }else{
        CLLocationCoordinate2D *coordinates = (CLLocationCoordinate2D*)malloc(locArrays.count * sizeof(CLLocationCoordinate2D));
        for (NSUInteger i=0; i < locArrays.count; i++) {
            trackdata *loa = (trackdata*)[locArrays objectAtIndex:i];
            coordinates[i].longitude = [loa.lon integerValue]/1000000.0 ;
            coordinates[i].latitude  = [loa.lat integerValue]/1000000.0 ;
            
            CLLocationCoordinate2D point = {coordinates[i].latitude , coordinates[i].longitude};
            NSString *titleStr = @"";
            if (i == 0) {
                titleStr = @"起点";
            } else if (i == locArrays.count - 1){
                titleStr = @"终点";
            }else{
                titleStr = @"途经点";
            }
            [self addAnnotationWithCooordinate:point withTitle:titleStr calloutText: [NSString stringWithFormat:@"%@  速度%@km/h",loa.satelliteTimeStr,loa.speed]];
        }

        MAPolyline *polyline = [MAPolyline polylineWithCoordinates:coordinates count:locArrays.count];
        //    free(coordinates), coordinates = NULL;
        [self.mapView addOverlay:polyline];
        self.mapView.visibleMapRect = polyline.boundingMapRect;
        CLLocationCoordinate2D end = {coordinates[1].latitude , coordinates[1].longitude};
        [self.mapView setCenterCoordinate:end];
        [self.mapView setZoomLevel:16.1 animated:YES];
    }
}
#pragma mark -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1573 && buttonIndex == 1) {
        //电子围栏
        if ([self.locModel.data.isOpenVf  integerValue] ==1) {
            //关闭电子围栏
            NSString *vfUrl = [NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,CLOSEVF,[[PublicFunction ShareInstance]getAccount].data.carId];
            [GNETS_NetWorkManger GetJSONWithUrl:vfUrl isNeedHead:YES success:^(NSDictionary *jsonDic) {
                                NSUInteger code = [[jsonDic objectForKey:@"code"] integerValue];
                                if (code == 1) {
                                    //刷新地图 去除围栏
                                    self.locModel.data.isOpenVf = @0;
                                    [self.mapView removeOverlays:self.mapView.overlays];
                                    [JKPromptView showWithImageName:nil message:@"电子围栏关闭成功"];
                                    [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_open"] forState:0];
                                }
            } fail:^{
                
            }];
        }else{
            //开启电子围栏
            [self openCarVf];
        }

    }
}
#pragma mark - 车辆信息
- (void)getCarInfo:(UIButton *)btn
{
    btn.transform = CGAffineTransformRotate(btn.transform, REES_TO_RADIANS(180));
    float width = btn.frame.size.width;
    float height = btn.frame.size.height;

    UIView  *maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    maskView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:maskView];
    WEAKSELF;
    [maskView whenTapped:^{
        [UIView animateWithDuration:0.2 animations:^{
            [weakSelf.msgView removeFromSuperview];
            [maskView removeFromSuperview];
            _infoBtn.transform =CGAffineTransformIdentity;
            [_infoBtn setBackgroundImage:[UIImage imageNamed:@"car_status"] forState:0];
        }];
    }];
    
    _msgView = [[MsgView alloc] initWithFrame:CGRectMake(btn.frame.origin.x+width-230, Orgin_y(btn)-height -200, 230, 200) With:self.locModel.data WithLocalCoordinate:_center];
    [self.view addSubview:_msgView];
    [_infoBtn setBackgroundImage:[UIImage imageNamed:@"close_map_Type_tip"] forState:0];
    
}
#pragma mark - 改变地图显示
- (void)changeMapTyleFinder:(UIButton *)btn
{
    float width = btn.frame.size.width;
    
    UIView  *maskView = [[UIView alloc] initWithFrame:self.view.bounds];
    maskView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:maskView];
    WEAKSELF;
    [maskView whenTapped:^{
        [UIView animateWithDuration:0.2 animations:^{
            [weakSelf.mapTypeView removeFromSuperview];
            [maskView removeFromSuperview];
            [_mapTypeBtn setBackgroundImage:[UIImage imageNamed:@"map_switch"] forState:0];
        }];

    }];
    
    _mapTypeView = [[MapTypeView alloc] initWithFrame:CGRectMake(btn.frame.origin.x+width-223, Orgin_y(btn), 223, 100)];
    _mapTypeView.delegate = self;
    [self.view addSubview:_mapTypeView];
    [_mapTypeBtn setBackgroundImage:[UIImage imageNamed:@"close_map_Type_tip"] forState:0];
    
}
#pragma mark - MapTypeViewPro
- (void)changeMAMapType:(NSString *)type
{
    if ([type isEqualToString:@"卫星图"]) {
        self.mapView.mapType = MAMapTypeSatellite;
    }else{
        self.mapView.mapType = MAMapTypeStandard;
    }
}
#pragma mark -是否显示路况
- (void)showTrafficEvent:(UIButton *)btn
{
    NSString *str =[USER_D objectForKey:@"showTraffic"];
    if (str.length>0) {
        if ([str isEqualToString:@"YES"]) {
            self.mapView.showTraffic = NO;
            [btn setBackgroundImage:[UIImage imageNamed:@"traffic_close"] forState:0];
        }else{
            self.mapView.showTraffic = YES;
            [btn setBackgroundImage:[UIImage imageNamed:@"traffic_open"] forState:0];
        }
    }else {
        self.mapView.showTraffic = YES;
        [btn setBackgroundImage:[UIImage imageNamed:@"traffic_open"] forState:0];
    }
    NSString *trafficString = self.mapView.showTraffic?@"YES":@"NO";
    [USER_D setObject:trafficString forKey:@"showTraffic"];
    [USER_D synchronize];
}
#pragma mark -导航
- (void)startCarNav{
    if (_userLocation)
    {
        _startPoint = [AMapNaviPoint locationWithLatitude:_userLocation.coordinate.latitude
                                                longitude:_userLocation.coordinate.longitude];
        NavMapViewController *navVC = [[NavMapViewController alloc] init]; //开启导航
        navVC.startPoint = _startPoint;
        navVC.delegate = self;
        navVC.navBlock = ^{
                [_iFlySpeechSynthesizer startSpeaking:@"计算路程失败，请您重试"];
        };
        navVC.endPoint = _endPoint;
        [self presentViewController:navVC animated:NO completion:nil];
    }else{
        [_iFlySpeechSynthesizer startSpeaking:@"手机定位尚未完成，请您在地图上手动选择位置"];
        //选择起点
         _singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        _singleTap.cancelsTouchesInView = NO;
        [self.mapView addGestureRecognizer:_singleTap];
    }
}

#pragma mark -CustomAnnotationViewPro 自定义代理
- (void)tapClickWith:(MAAnnotationView *)annptations WithTag:(NSUInteger)tagz{
    
    if (tagz == 2002) {
        _startPoint = [AMapNaviPoint locationWithLatitude:_touchCll.latitude longitude:_touchCll.longitude];
            NavMapViewController *navVC = [[NavMapViewController alloc] init]; //开启导航
            navVC.startPoint = _startPoint;
            navVC.delegate = self;
        navVC.navBlock = ^{
        [_iFlySpeechSynthesizer startSpeaking:@"计算路程失败，请您重试"];

        };
            navVC.endPoint = _endPoint;
            [self presentViewController:navVC animated:NO completion:nil];
    }
}
#pragma mark -定位失败后 点击地图选择起点
- (void)tapPress:(UIGestureRecognizer*)gestureRecognizer {
    
    // 移除上一个标注的大头针
    if (_removeCll.latitude) {
        NSArray *annArrays = _mapView.annotations;
        for ( MAPointAnnotation *csanotation in annArrays) {
            if (csanotation.coordinate.latitude == _removeCll.latitude && csanotation.coordinate.longitude == _removeCll.longitude) {
                [_mapView removeAnnotation:csanotation];
            }
        }
    }
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];//这里touchPoint是点击的某点在地图控件中的位置
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];//这里touchMapCoordinate就是该点的经纬度了
    
    _touchCll = touchMapCoordinate;
    _removeCll = _touchCll;
    
    MAPointAnnotation *annotation = [[MAPointAnnotation alloc] init];
    annotation.coordinate = touchMapCoordinate;
    [self.mapView addAnnotation:annotation];
}
- (void)dismissEvents{
    // 移除上一个标注的大头针

    if (_removeCll.latitude) {
        NSArray *annArrays = _mapView.annotations;
        for ( MAPointAnnotation *csanotation in annArrays) {
            if (csanotation.coordinate.latitude == _removeCll.latitude && csanotation.coordinate.longitude == _removeCll.longitude) {
                [_mapView removeAnnotation:csanotation];
            }
        }
    }
}

#pragma mark - IFlySpeechSynthesizerDelegate
- (void)initIFlySpeech
{
    if (self.iFlySpeechSynthesizer == nil)
    {
        _iFlySpeechSynthesizer = [IFlySpeechSynthesizer sharedInstance];
    }
    
    _iFlySpeechSynthesizer.delegate = self;
}
#pragma mark - iFlySpeechSynthesizer Delegate

- (void)onCompleted:(IFlySpeechError *)error
{
    DLog(@"Speak Error:{%d:%@}", error.errorCode, error.errorDesc);
}
#pragma mark -开启电子围栏
- (void)openCarVf{
    
    float lon = [self.locModel.data.lon floatValue]/1000000.0;
    float lat = [self.locModel.data.lat floatValue]/1000000.0;
    
    float r = 100;
    float k = 111700*2;
    float R = 3.141592654/180;
    //计算电子围栏的范围
    float top = lat + (r/k);//最大纬度
    float bottom = lat - r/k;//最小纬度
    float left = lon - r/(k*cos(lat*R));//最小经度
    float right = lon + r/(k*cos(lat*R));//最大经度
    
    NSString *openUrl = [NSString stringWithFormat:@"%@%@?carId=%@&lon=%f&lat=%f&maxLon=%f&maxLat=%f&minLon=%f&minLat=%f",kProjectBaseUrl,OPENVF,[[PublicFunction ShareInstance]getAccount].data.carId,lon,lat,top,right,bottom,left];
    [GNETS_NetWorkManger GetJSONWithUrl:openUrl isNeedHead:YES success:^(NSDictionary *jsonDic) {
        NSUInteger code = [[jsonDic objectForKey:@"code"] integerValue];
        if (code != 1) {
            return ;
        }
        NSString *msgStr = jsonDic[@"errmsg"];
        [JKPromptView showWithImageName:nil message:msgStr];
        //重新请求
        [self loadCarLocation];
        
//        self.locModel.data.isOpenVf = @1;
//        [self drawOverlays];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [JKPromptView showWithImageName:nil message:@"电子围栏开启成功"];
//        });
//         [self.OverlayBtn setBackgroundImage:[UIImage imageNamed:@"fence_close"] forState:0];
        
    } fail:^{
        
    }];
}
- (void)drawOverlays
{
    DLog(@"输出有多少个蔚蓝－－－－－%ld",(unsigned long)self.mapView.overlays.count);
    [self.mapView removeOverlays:self.mapView.overlays];
    
    if([[self.mapView overlays] count] <1) {
        NSUInteger lat = [self.locModel.data.vfLat integerValue];
        NSUInteger lon = [self.locModel.data.vfLon integerValue];
        self.overlays = [NSMutableArray array];
        /* Circle. */
        MACircle *circle = [MACircle circleWithCenterCoordinate:CLLocationCoordinate2DMake(lat/1000000.0, lon/1000000.0) radius:100];
        [self.overlays insertObject:circle atIndex:0];
        [self.mapView addOverlay:circle];
    }
}
#pragma mark - Life Cycle
#pragma mark - Life Cycle
- (MACircleView *)mapView:(MAMapView *)mapView viewForOverlay:(id<MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[MACircle class]])
    {
        MACircleView *polylineView = [[MACircleView alloc] initWithCircle:overlay];
        
        polylineView.lineWidth   = 2.f;
        polylineView.strokeColor = [UIColor redColor];
        polylineView.fillColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        
        return polylineView;
    }
    if ([overlay isKindOfClass:[MAPolyline class]])
    {
        MAPolylineView *polylineView = [[MAPolylineView alloc] initWithPolyline:overlay];
        
        polylineView.lineWidth   = 2.f;
        polylineView.strokeColor = [UIColor colorWithRed:42/255.0 green:172/255.0 blue:247/255.0 alpha:1];
        
        return polylineView;
    }
    
    return nil;
}

#pragma mark -在同一视野内MKCoordinateRegionMake
- (void)zoomToMapPoints:(MAMapView*)mapView annotations:(NSArray*)annotations
{
    double minLat = 360.0f, maxLat = -360.0f;
    double minLon = 360.0f, maxLon = -360.0f;
    for (MAPointAnnotation *annotation in annotations) {
        if ( annotation.coordinate.latitude < minLat ) minLat = annotation.coordinate.latitude;
        if ( annotation.coordinate.latitude > maxLat ) maxLat = annotation.coordinate.latitude;
        if ( annotation.coordinate.longitude < minLon ) minLon = annotation.coordinate.longitude;
        if ( annotation.coordinate.longitude > maxLon ) maxLon = annotation.coordinate.longitude;
    }
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((minLat + maxLat) / 2.f, (minLon + maxLon) / 2.f);
    MACoordinateSpan span = MACoordinateSpanMake(maxLat - minLat, maxLon - minLon);
    MACoordinateRegion region = MACoordinateRegionMake(center, span);
    [_mapView setRegion:region animated:YES];
}
#pragma mark -UIButtonEvent
- (void)leftBtnAction
{
    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:^(BOOL finished) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
