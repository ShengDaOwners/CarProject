//
//  CustomAnnotationView.h
//  CustomAnnotationDemo
//
//  Created by songjian on 13-3-11.
//  Copyright (c) 2013年 songjian. All rights reserved.
//

#import <AMapNaviKit/MAAnnotationView.h>
#import <AMapNaviKit/MAAnnotation.h>
@protocol CustomAnnotationViewPro <NSObject>

- (void)tapClickWith:(MAAnnotationView *)annptations WithTag:(NSUInteger)tagz;

@end

@interface CustomAnnotationView : MAAnnotationView

@property (nonatomic,weak)id<CustomAnnotationViewPro>delegate;

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSUInteger tagz;


@property (nonatomic, copy) NSString *calloutText;

@property (nonatomic, strong) UIImage *portrait;

@property (nonatomic, strong) UIView *calloutView;

@end
