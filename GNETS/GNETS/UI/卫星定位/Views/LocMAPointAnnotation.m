//
//  LocMAPointAnnotation.m
//  GNETS
//
//  Created by cqz on 16/3/5.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "LocMAPointAnnotation.h"

@implementation LocMAPointAnnotation

@synthesize title = _title;

@synthesize subtitle = _subtitle;

@synthesize imageName = _imageName;

@synthesize selected = _selected;


@end
