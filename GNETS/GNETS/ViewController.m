//
//  ViewController.m
//  GNETS
//
//  Created by tcnj on 16/2/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//
#import "GNETSNaigationController.h"
#import "ViewController.h"
#import "GPSViewController.h"
#import "WarningMsgVC.h"
#import "StatisticalVC.h"
#import "LoginViewController.h"
#import "UserModel.h"
#import "Userdata.h"
#import "NSString+MD5.h"
#import "JPUSHService.h"
#import "GNETSMenuVC.h"
#import "MMDrawerController.h"
#import "MMDrawerVisualState.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@property (nonatomic, strong) UIImageView *StartImgView;//启动图
@property (nonatomic, strong) UIWindow *window;

@property (nonatomic,strong) MMDrawerController * drawerController;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self initView];
}
- (void)initView
{

    //启动图
    self.StartImgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.StartImgView.image = [UIImage imageNamed:@"Default.png"];
    [self.view addSubview:self.StartImgView];
    
    self.window = [UIApplication sharedApplication].windows[0];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    /*
     判断是否自动登录
     */
    [self isAutoLogin];
    
//    [self layOutTheApp];
}
- (void)isAutoLogin
{
    NSString *userName = [USER_D objectForKey:@"user_phone"];

    if (userName.length>0)
    {
        NSString *userpassWord =[USER_D objectForKey:@"user_password"];
        [SVProgressHUD showWithStatus:@"加载中" maskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD dismiss];

        WEAKSELF
        UIDevice *device = [UIDevice currentDevice];
        NSString *deviceUDID = [NSString stringWithFormat:@"%@",device.identifierForVendor];
        DLog(@"输出设备的id---%@",deviceUDID);
        NSArray *array = [deviceUDID componentsSeparatedByString:@">"];
        NSString *udidStr = array[1];
        DLog(@"设备标识符:%@",udidStr);
        NSString *tempStr = [udidStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *aliasString = [ConFunc trimStringUUID:(NSMutableString *)tempStr];

        NSString *loginUrl = [[NSString stringWithFormat:@"%@%@?loginName=%@&password=%@&system=%@&clientId=%@",kProjectBaseUrl,LOGINURL,userName,[userpassWord md5],[NSString stringWithFormat:@"iOS%@",device.systemVersion],aliasString] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [GNETS_NetWorkManger GetJSONWithUrl:loginUrl isNeedHead:NO  success:^(NSDictionary *jsonDic) {
            
            [PublicFunction showErrorMsg:jsonDic];

            UserModel *model = [[UserModel alloc] initWithDictionary:jsonDic];
            
            if ([model.code intValue] !=1) {
                [SVProgressHUD showErrorWithStatus:model.errmsg];
                [USER_D removeObjectForKey:@"user_phone"];
                [USER_D removeObjectForKey:@"user_password"];
                [USER_D synchronize];
                
                [self LoginAction];
            }else{
                [JPUSHService setTags:nil alias:aliasString fetchCompletionHandle:^(int iResCode, NSSet *iTags, NSString *iAlias){
                    NSLog(@"rescode: %d, \ntags: %@, \nalias: %@\n", iResCode, iTags, iAlias);
                }];

                [PublicFunction ShareInstance].userToken = model.data.userToken;
                [PublicFunction ShareInstance].m_user = model;
                //存用户信息
                [[PublicFunction ShareInstance]loginSuccessWithAccount:jsonDic];
                [weakSelf layOutTheApp];

            }
            
        } fail:^{
            [SVProgressHUD showErrorWithStatus:@"请求失败"];
            //挑转到登录页面
            [self LoginAction];
        }];
    }else{
        [self LoginAction];
    }
    
}
- (void)LoginAction
{
    LoginViewController *login = [[LoginViewController alloc] init];
    GNETSNaigationController *nav =[[GNETSNaigationController alloc] initWithRootViewController:login];
    nav.navigationBarHidden = YES;
    UIWindow *window =[[[UIApplication sharedApplication] windows] objectAtIndex:0];
    window.rootViewController = nav;
}
#pragma mark -布局tabbar
- (void)layOutTheApp
{
    UITabBarController *tabbar = [[UITabBarController alloc] init];
    //设定Tabbar的点击后的颜色 #ffa055
    [[UITabBar appearance] setTintColor:[UIColor colorWithHexString:@"#0096D7"]];
    // [[UITabBar appearance] setBackgroundColor:[UIColor colorWithHexString:@"#373737"]];
    [[UITabBar appearance] setBackgroundImage:[ConFunc createImageWithColor:RGBACOLOR(252, 252, 252, 1)
                                                                       size:CGSizeMake(kMainScreenWidth,kTabBarHeight)]];//设置背景，修改颜色是没有用的
    
    //设定Tabbar的颜色
    [[UITabBar appearance] setBarTintColor:[UIColor whiteColor]];
    UINavigationController *gpsVC = [self newNavigationControllerForClass:[GPSViewController class]
                                                                   title:@"卫星定位"
                                                               itemImage:@"location_ico"
                                                           selectedImage:@"location_ico_hover"];
    UINavigationController *statisticalvc = [self newNavigationControllerForClass:[StatisticalVC class]
                                                                    title:@"每日统计"
                                                                itemImage:@"char_ico"
                                                            selectedImage:@"chart_ico_hover"];
    
    UINavigationController *warnVC = [self newNavigationControllerForClass:[WarningMsgVC class]
                                                                     title:@"报警消息"
                                                                 itemImage:@"alarm_ico"
                                                             selectedImage:@"alarm_ico_hover"];

    
    tabbar.viewControllers = @[gpsVC,statisticalvc,warnVC];
    
    GNETSNaigationController *navigationController = [[GNETSNaigationController alloc] initWithRootViewController:tabbar];
    GNETSMenuVC *menuController = [GNETSMenuVC new];

    UINavigationController * leftSideNavController = [[GNETSNaigationController alloc] initWithRootViewController:menuController];
    [leftSideNavController setRestorationIdentifier:@"MMExampleLeftNavigationControllerRestorationKey"];
    
    self.drawerController = [[MMDrawerController alloc]
                             initWithCenterViewController:navigationController
                             leftDrawerViewController:leftSideNavController
                             rightDrawerViewController:nil];
    [self.drawerController setShowsShadow:YES];
    [self.drawerController setMaximumLeftDrawerWidth:leftWidth];
    [self.drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self.drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    self.window.rootViewController = self.drawerController;
}
- (GNETSNaigationController *)newNavigationControllerForClass:(Class)controllerClass
                                                      title:(NSString *)title
                                                  itemImage:(NSString *)itemImage
                                              selectedImage:(NSString *)selectedImage
{
    UIViewController *viewController = [[controllerClass alloc] init];
    GNETSNaigationController *theNavigationController = [[GNETSNaigationController alloc]
                                                       initWithRootViewController:viewController];
    theNavigationController.tabBarItem.title = title;
    theNavigationController.tabBarItem.image = [UIImage imageNamed:itemImage];
    theNavigationController.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    theNavigationController.navigationBarHidden = YES;
    return theNavigationController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
