//
//  DefineHeader.h
//  GNETS
//
//  Created by tcnj on 16/2/16.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#ifndef DefineHeader_h
#define DefineHeader_h

/**
 *
 * color macros
 *
 **/
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define RGBACOLOR(r,g,b,a)   [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]


#define orange  @"#e64708"  //底色橘红色
#define gray    @"#373737"  //底色浅灰色
#define ligtgray @"#666666" //浅灰色
#define navColor @"#0096D7"
/**
 *
 * REGEX macros
 *
 **/
//@"^1[3|4|5|8][0-9]\\d{8}$"
#define REGEX_PHONE         @"^\\d{11}$"
#define REGEX_EMAIL         @"^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*\" + \"+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?"
#define REGEX_NAME          @"(^[\u4e00-\u9fa5]|[a-z]|[A-Z])[\u4e00-\u9fa5|a-z|A-Z|0-9|\\.|_|\\s]{0,14}[\u4e00-\u9fa5|a-z|A-Z|0-9|\\.|_]$"
#define REGEX_REALNAME      @"^[^x00-xff]{2,20}$"
#define REGEX_CARD          @"^(\\d{15}|\\d{17}[\\dx])$"
#define REGEX_ZZS           @"^[0-9]\\d*$"
#define REGEX_YHK           @"^(\\d{16}|\\d{19})$"

#import "PNChart.h"
#import "Userdata.h"
#import "UserModel.h"
#import "MJPhotoBrowser.h"
#import "MJPhoto.h"
#import "GPDateView.h"
#import "GNETSSqlitManager.h"
#import "JKPromptView/JKPromptView.h"

/**
 *
 * frame macros
 *
 **/
#define iphone5_W 320
//设备屏幕frame
#define kMainScreenFrameRect                          [[UIScreen mainScreen] bounds]
//状态栏高度
#define kMainScreenStatusBarFrameRect                 [[UIApplication sharedApplication] statusBarFrame]
#define kMainScreenHeight                             kMainScreenFrameRect.size.height
#define kMainScreenWidth                              kMainScreenFrameRect.size.width
//减去状态栏和导航栏的高度
#define kScreenHeightNoStatusAndNoNaviBarHeight       (kMainScreenFrameRect.size.height - kMainScreenStatusBarFrameRect.size.height-44.0f)
//减去状态栏和底部菜单栏高度
#define kScreenHeightNoStatusAndNoTabBarHeight       (kMainScreenFrameRect.size.height - kMainScreenStatusBarFrameRect.size.height-49.0f)
//减去状态栏和底部菜单栏以及导航栏高度
#define kScreenHeightNoStatusAndNoTabBarNoNavBarHeight       (kMainScreenFrameRect.size.height - kMainScreenStatusBarFrameRect.size.height-49.0f - 44.0f)
//底部工具栏高度
#define kTabBarHeight               49
//导航栏高度
#define kNavBarHeight               44

#define GET(str)                    (str ? str : @"")

/**
 *
 * font
 macros
 *
 **/
#define Font_12        [UIFont  systemFontOfSize:12]
#define Font_13        [UIFont  systemFontOfSize:13]
#define Font_14        [UIFont  systemFontOfSize:14]
#define Font_15        [UIFont  systemFontOfSize:15]
#define Font_16        [UIFont  systemFontOfSize:16]
#define Font_18        [UIFont  systemFontOfSize:18]
#define Font_20        [UIFont systemFontOfSize:20]

#define Orgin_y(container)   (container.frame.origin.y+container.frame.size.height)
#define Orgin_x(container)   (container.frame.origin.x+container.frame.size.width)

#define lineColor [UIColor colorWithR:226 g:226 b:226 a:1]
#define line_w 0.5
#define isTest 0

#define DATE_FORMAT_YMDHMS             @"yyyy-MM-dd HH:mm:ss"
#define DATE_FORMAT_YMDHM               @"yyyy-MM-dd HH:mm"

/**
 * NSLog宏，限定仅在Debug时才打印,release不打印，防止拖慢程序运行
 */
#ifdef DEBUG
#define DLog(...) NSLog(__VA_ARGS__)
#else
#define DLog(...)
#endif

//当前设备是否为 iPhone5
#define IS_IPHONE5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//宏定义方法
#define SHOW_ALERT(msg) UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:msg delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];\
[alert show];\


#define WEAKSELF typeof(self) __weak weakSelf = self;

#define USER_D [NSUserDefaults standardUserDefaults]

/**
 *
 * system  macros
 *
 **/

#define CurrentSystemVersion ([[[UIDevice currentDevice] systemVersion] floatValue])
#define IOS_VERSION_ABOVE_8                     (([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) ? (YES) : (NO))
#define IOS_VERSION_ABOVE_7                     (([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) ? (YES) : (NO))
#define IOS_VERSION_ABOVE_7_1                     (([[UIDevice currentDevice].systemVersion floatValue] >= 7.1) ? (YES) : (NO))
#define IOS_VERSION_6                           (([[UIDevice currentDevice].systemVersion floatValue] < 7.0 && [[UIDevice currentDevice].systemVersion floatValue] >= 6.0) ? (YES) : (NO))


typedef void(^ZDBlock)(void);
typedef void(^ZDBlockBlock)(ZDBlock block);
typedef void(^ZDObjectBlock)(id obj);
typedef void(^ZDArrayBlock)(NSArray *array);
typedef void(^ZDMutableArrayBlock)(NSMutableArray *array);
typedef void(^ZDDictionaryBlock)(NSDictionary *dic);
typedef void(^ZDErrorBlock)(NSError *error);
typedef void(^ZDIndexBlock)(NSInteger index);
typedef void(^ZDFloatBlock)(CGFloat afloat);
typedef void(^ZDStringBlock)(NSString *string);


typedef void(^ZDCancelBlock)(id viewController);
typedef void(^ZDFinishedBlock)(id viewController, id object);

typedef void(^ZDSendRequestAndResendRequestBlock)(id sendBlock, id resendBlock);



#endif /* DefineHeader_h */
