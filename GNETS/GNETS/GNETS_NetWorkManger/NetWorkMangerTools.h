//
//  NetWorkMangerTools.h
//  GNETS
//
//  Created by cqz on 16/3/6.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetWorkMangerTools : NSObject

//#pragma -mark 执行远程锁车命令
//+ (void)lockCarRequestSuccess:(void (^)())success
//                         fail:(void (^)())fail;
//#pragma -mark 执行远程解锁命令
//+ (void)unlockCarRequestSuccess:(void (^)())success
//                           fail:(void (^)())fail;
//
#pragma mark -查看报警消息
+ (void)lookViewAlarmWithEventId:(NSString *)eventId;
@end
