//
//  GNETS_NetWorkManger.m
//  GNETS
//
//  Created by tcnj on 16/2/17.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "GNETS_NetWorkManger.h"
#import "AFNetworking.h"

@implementation GNETS_NetWorkManger

#pragma mark 检测网路状态
+ (void)netWorkStatus
{
    /**
     AFNetworkReachabilityStatusUnknown          = -1,  // 未知
     AFNetworkReachabilityStatusNotReachable     = 0,   // 无连接
     AFNetworkReachabilityStatusReachableViaWWAN = 1,   // 3G 花钱
     AFNetworkReachabilityStatusReachableViaWiFi = 2,   // WiFi
     */
    // 如果要检测网络状态的变化,必须用检测管理器的单例的startMonitoring
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    // 检测网络连接的单例,网络变化时的回调方法
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         //DLog(@"%d", status);
     }];
}
#pragma mark GET请求
+ (void)GetJSONWithUrl:(NSString *)url isNeedHead:(BOOL)neeadHead success:(void (^)(NSDictionary *jsonDic))success fail:(void (^)())fail
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置超时时间
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    // 设置请求格式
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    // 设置返回格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //增加请求头代码
    if (neeadHead && [[PublicFunction ShareInstance] getAccount].data.userToken) {
        [manager.requestSerializer setValue:[[PublicFunction ShareInstance] getAccount].data.userToken forHTTPHeaderField:@"Authorization"];
//            [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"X-AUTH-TOKEN"];

    }
//    [manager.requestSerializer setValue:[ConFunc getBuildVersion] forHTTPHeaderField:@"version"];
//    [manager.requestSerializer setValue:@"1" forHTTPHeaderField:@"app"];
    
    NSDictionary *dict = @{@"format": @"json"};
    
    // 网络访问是异步的,回调是主线程的,因此不用管在主线程更新UI的事情
    
    [manager GET:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success) {
            NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            DLog(@"success[Get]请求地址[%@]",url);
            DLog(@"success[Get]请求返回数据\n %@",result);
            NSData *data = operation.responseData;
            NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            success(dict);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        DLog(@"%@", error);
        if (fail) {
            fail(error);
        }
    }];
    
}

#pragma mark POST请求
+ (void)PostJSONWithUrl:(NSString *)urlStr parameters:(NSDictionary *)parameters isNeedHead:(BOOL)neeadHead success:(void (^)(NSDictionary *jsonDic))success fail:(void (^)())fail
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    // 设置请求格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    // 设置返回格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //增加请求头代码
    if (neeadHead&&[[PublicFunction ShareInstance] getAccount].data.userToken) {
        //    [manager.requestSerializer setValue:[ConFunc getBuildVersion] forHTTPHeaderField:@"version"];
        [manager.requestSerializer setValue:[[PublicFunction ShareInstance] getAccount].data.userToken forHTTPHeaderField:@"Authorization"];
    }

    
    
    // 设置超时时间
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 10.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    
    [manager POST:urlStr
       parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         DLog(@"success[POST]请求参数[%@]\n %@",urlStr,parameters);
         DLog(@"success[POST]请求返回数据\n %@",result);
         
         if (success)
         {
             NSData *data = operation.responseData;
             NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             success(dict);
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         DLog(@"error[POST]请求参数[%@]\n %@",urlStr,parameters);
         DLog(@"error[POST]请求返回数据 %@",error);
         
         if (fail)
         {
             fail(error);
         }
     }];
    
}

#pragma mark 上传多张图片数组
+ (void)postUploadWithUrl:(NSString *)urlStr
               parameters:(NSDictionary *)parameters
               WithImgDic:(NSDictionary *)allImgDic
                  success:(void (^)(NSDictionary * jsonDic))success
                     fail:(void (^)())fail
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    // 设置超时时间
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 60.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    // 设置请求格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    // 设置返回格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setValue:[[PublicFunction ShareInstance] getAccount].data.userToken forHTTPHeaderField:@"Authorization"];

    
    [manager POST:urlStr parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         //上传图片数组 多张图片
         for (NSString *key in [allImgDic allKeys])
         {
             //UIImage *image = [allImgDic objectForKey:key];
             NSData *imageData =[allImgDic objectForKey:key];// UIImageJPEGRepresentation(image, 0.5);
             NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
             formatter.dateFormat = @"yyyyMMddHHmmss";
             NSString *str = [formatter stringFromDate:[NSDate date]];
             NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
             // 上传的 ，FileData二进制数据 ，参数名 name ， 文件名fileName，图片类型mimeType
             //[formData appendPartWithFormData:nil name:nil];
             [formData appendPartWithFileData:imageData name:key fileName:fileName mimeType:@"image/jpeg/png/jpg"];
         }
         
     }
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSData *data = operation.responseData;
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
         success(dict);
         NSString *result = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
         DLog(@"完成 %@", result);
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         fail(error);
         DLog(@"错误 %@", error.localizedDescription);
     }];
}
#pragma mark上传单张图片
+(void)postOneImgUrlString:(NSString *)url
                    msgDic:(NSDictionary *)msgDic
                     image:(UIImage *)image
                   success:(void (^)(NSDictionary *))success
                      fail:(void (^)())fail
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    // 设置超时时间
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 60.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    // 设置请求格式
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    // 设置返回格式
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //增加请求头代码
//    [manager.requestSerializer setValue:[ConFunc getBuildVersion] forHTTPHeaderField:@"version"];
//    [manager.requestSerializer setValue:@"1" forHTTPHeaderField:@"app"];
    
    
    
    DLog(@"参数是－－－－－%@",msgDic);
    
    [manager POST:url
       parameters:msgDic
constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    NSString *fileName = [NSString stringWithFormat:@"%@.jpg", str];
    
    // 上传图片，以文件流的格式
    [formData appendPartWithFileData:imageData
                                name:@"file"
                            fileName:fileName
                            mimeType:@"image/jpeg/png/jpg"];
    
} success:^(AFHTTPRequestOperation *operation, id responseObject) {
    NSString *result = [[NSString alloc] initWithData:operation.responseData encoding:NSUTF8StringEncoding];
    DLog(@"[POST]请求地址[%@]\n %@",url,result);
    NSData *data = operation.responseData;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    success(dict);
} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    fail(error);
}];
    
}
+ (void)setFormDataWithUrl:(NSString  *)url
                  WithPara:(NSDictionary *)para
           WithFileDataDic:(NSDictionary *)fileDataDic
        WithFileNamePrefix:(NSString *)fileNamePrefix
                   success:(void (^)(NSDictionary *dic))success
                      fail:(void (^)())fail{
    
    
    //    NSString *boundary = [NSString stringWithFormat:@"Boundary+%08X%08X", arc4random(), arc4random()];
    NSMutableData *body = [NSMutableData data];
    
    NSString *BOUNDARY = @"0xKhTmLbOuNdArY";
    
    
    /** 遍历字典将字典中的键值对转换成请求格式:
     --Boundary+72D4CD655314C423
     Content-Disposition: form-data; name="empId"
     
     254
     --Boundary+72D4CD655314C423
     Content-Disposition: form-data; name="shopId"
     
     18718
     */
    //表单数据 param
    [para enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        NSMutableString *fieldStr = [NSMutableString string];
        [fieldStr appendString:[NSString stringWithFormat:@"--%@\r\n", BOUNDARY]];
        [fieldStr appendString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key]];
        [fieldStr appendString:[NSString stringWithFormat:@"%@", obj]];
        [body appendData:[fieldStr dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    //文件逻辑
    for(NSString *key in fileDataDic) {
        NSData *fileData = [fileDataDic objectForKey:key];
        NSString *param = [NSString stringWithFormat:@"--%@\r\nContent-Disposition: form-data; name=\"%@\";filename=\"%@\"\r\nContent-Type: application/octet-stream\r\n\r\n",BOUNDARY,[NSString stringWithFormat:@"%@%@",fileNamePrefix,key],key,nil];
        [body appendData:[param dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:fileData];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:0 timeoutInterval:5.0f];
    
    NSString *endString = [NSString stringWithFormat:@"--%@--",BOUNDARY];
    [body appendData:[endString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    
    // 设置请求类型为post请求
    request.HTTPMethod = @"post";
    // 设置request的请求体
    request.HTTPBody = body;
    // 设置头部数据，标明上传数据总大小，用于服务器接收校验
    [request setValue:[NSString stringWithFormat:@"%ld", body.length] forHTTPHeaderField:@"Content-Length"];
    // 设置头部数据，指定了http post请求的编码方式为multipart/form-data（上传文件必须用这个）。
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@",BOUNDARY] forHTTPHeaderField:@"Content-Type"];
    //token 放请求头
    [request setValue:[[PublicFunction ShareInstance] getAccount].data.userToken forHTTPHeaderField:@"Authorization"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[[NSOperationQueue alloc]init] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        if (data) {
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"Result--%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            if (!connectionError) {
                success(dic);
            }
        }else{
            success(nil);
        }

        
    }];
    
    
}
//- (BOOL) connectedToNetwork
//{
//    // 创建零地址，0.0.0.0的地址表示查询本机的网络连接状态
//    struct sockaddr_in zeroAddress;//sockaddr_in是与sockaddr等价的数据结构
//    bzero(&zeroAddress, sizeof(zeroAddress));
//    zeroAddress.sin_len = sizeof(zeroAddress);
//    zeroAddress.sin_family = AF_INET;//sin_family是地址家族，一般都是“AF_xxx”的形式。通常大多用的是都是AF_INET,代表TCP/IP协议族
//
//    /**
//     *  SCNetworkReachabilityRef: 用来保存创建测试连接返回的引用
//     *
//     *  SCNetworkReachabilityCreateWithAddress: 根据传入的地址测试连接.
//     *  第一个参数可以为NULL或kCFAllocatorDefault
//     *  第二个参数为需要测试连接的IP地址,当为0.0.0.0时则可以查询本机的网络连接状态.
//     *  同时返回一个引用必须在用完后释放.
//     *  PS: SCNetworkReachabilityCreateWithName: 这个是根据传入的网址测试连接,
//     *  第二个参数比如为"www.apple.com",其他和上一个一样.
//     *
//     *  SCNetworkReachabilityGetFlags: 这个函数用来获得测试连接的状态,
//     *  第一个参数为之前建立的测试连接的引用,
//     *  第二个参数用来保存获得的状态,
//     *  如果能获得状态则返回TRUE，否则返回FALSE
//     *
//     */
//    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress); //创建测试连接的引用：
//    SCNetworkReachabilityFlags flags;
//
//    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
//    CFRelease(defaultRouteReachability);
//
//    if (!didRetrieveFlags)
//    {
//        printf("Error. Could not recover network reachability flagsn");
//        return NO;
//    }
//
//    /**
//     *  kSCNetworkReachabilityFlagsReachable: 能够连接网络
//     *  kSCNetworkReachabilityFlagsConnectionRequired: 能够连接网络,但是首先得建立连接过程
//     *  kSCNetworkReachabilityFlagsIsWWAN: 判断是否通过蜂窝网覆盖的连接,
//     *  比如EDGE,GPRS或者目前的3G.主要是区别通过WiFi的连接.
//     *
//     */
//    BOOL isReachable = ((flags & kSCNetworkFlagsReachable) != 0);
//    BOOL needsConnection = ((flags & kSCNetworkFlagsConnectionRequired) != 0);
//    return (isReachable && !needsConnection) ? YES : NO;
//}
//-(void)downloadWithrequest:(NSString *)urlString downloadpath:(NSString *)downloadpath downloadblock:(DownloadBlock)downloadblock {
//    if (!self.connected) {
//        return;
//    }
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
//    self.task = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
//        NSURL *desturl = [NSURL fileURLWithPath:downloadpath];
//        return desturl;
//    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
//        downloadblock(response,filePath,error);
//    }];
//    [self.task resume];
//}
////-(void)stopdownload {
////    [self.task suspend];
////}

@end
