//
//  NetWorkMangerTools.m
//  GNETS
//
//  Created by cqz on 16/3/6.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "NetWorkMangerTools.h"

@implementation NetWorkMangerTools

#pragma -mark 执行远程锁车命令
//+ (void)lockCarRequestSuccess:(void (^)())success
//                         fail:(void (^)())fail{
//    [SVProgressHUD showInfoWithStatus:@"正在关闭语音寻车，请稍等"];
//    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,LockCar,[[PublicFunction ShareInstance]getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
//        [SVProgressHUD dismiss];
//
//        if ([jsonDic[@"code"] intValue] == 1) {
//            success();
//            [JKPromptView showWithImageName:nil message:@"开启命令发送成功"];
//        }else{
//            fail();
//            [JKPromptView showWithImageName:nil message:@"开启命令发送失败"];
//        }
//        
//    } fail:^{
//        [SVProgressHUD dismiss];
//    }];
//}
//
//#pragma -mark 执行远程解锁命令
//+ (void)unlockCarRequestSuccess:(void (^)())success
//                           fail:(void (^)())fail{
//    [SVProgressHUD showInfoWithStatus:@"正在开启语音寻车，请稍等"];
//    [GNETS_NetWorkManger GetJSONWithUrl:[NSString stringWithFormat:@"%@%@?carId=%@",kProjectBaseUrl,UnLockCar,[[PublicFunction ShareInstance]getAccount].data.carId] isNeedHead:YES success:^(NSDictionary *jsonDic) {
//        /*
//         {
//         "code": 1,
//         "errmsg": "解锁命令发送成功"
//         }
//         */
//        [SVProgressHUD dismiss];
//        
//        if ([jsonDic[@"code"] intValue] == 1) {
//            success();
//            [JKPromptView showWithImageName:nil message:@"关闭命令发送成功"];
//
//        }else{
//            fail();
//            [JKPromptView showWithImageName:nil message:@"关闭命令发送失败"];
//        }
//        
//    } fail:^{
//        [SVProgressHUD dismiss];
//    }];
//}
#pragma mark -查看报警消息
+ (void)lookViewAlarmWithEventId:(NSString *)eventId
{
    NSString *lookUrl = [NSString stringWithFormat:@"%@%@?carId=%@&eventId=%@",kProjectBaseUrl,LookAlarmEvent,[[PublicFunction ShareInstance]getAccount].data.carId,eventId];
    [GNETS_NetWorkManger GetJSONWithUrl:lookUrl isNeedHead:YES success:^(NSDictionary *jsonDic) {
        
        
    } fail:^{
        
    }];
}


@end
