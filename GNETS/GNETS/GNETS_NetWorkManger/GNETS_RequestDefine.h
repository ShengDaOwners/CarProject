//
//  GNETS_RequestDefine.h
//  GNETS
//
//  Created by tcnj on 16/2/17.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#ifndef GNETS_RequestDefine_h
#define GNETS_RequestDefine_h


#endif /* GNETS_RequestDefine_h */

#define kProjectBaseUrl @"http://api.car.gnets.cn/app/"

/*
 1登录
 */
#define LOGINURL @"checkLogin.do"
/*
 2获取用户基本资料
 */
#define INFOURL @"car/getUserInfo.do"
/*
 3获取车辆基本资料
 */
#define CARINFO @"car/getCarInfo.do"
/*
 4用户注册
 */
#define REGISTERURL @"car/regDevice.do"
/*
 5根据用户输入IMEI后八位自动补全IMEI
 */
#define IMEIURLSTRING @"car/searchCarLikeImei.do"
/*
 6 验证数据卡号
 */
#define ValidationTelNum  @"car/checkTelNum.do"
/*
 7获取每日统计数据
 */
#define StaticSofEveryDay @"chart/getDayData.do"
/*
 8获取指定天数的统计数据
 */
#define StaticSectionDateInfo @"chart/getSomeDayData.do"
/*
 9开启电子围栏
 */
#define OPENVF @"vf/openVf.do"
/*
 10执行关闭电子围栏
 */
#define CLOSEVF @"vf/closeVf.do"
/*
 11获取车辆位置信息
 */
#define CarLocationInfo @"map/getLocInfo.do"
/*
 12根据起止时间获取轨迹信息
 */
#define SEARCHTRACK @"map/searchTrack.do"
/*
  13获取报警消息
 */
#define AlarmEventInfo @"alarm/getNewAlarmEventInfo.do"
/*
 14查看报警消息
 */
#define LookAlarmEvent @"/alarm/viewAlarmEvent.do"

/*
  15在线预订
 */
#define ONLINEBOOK @"book/saveOnlineBook.do"
/*
 16退出登录
 */
#define Logout @"logout.do"
/*
 17修改基本资料
 */
#define UpdateUser  @"car/updateUser.do"
/*
 18 修改车辆资料
 */
#define UpdateCar   @"car/updateCar.do"

/*
 19 服务条款
 */
#define Service_Terms  @"http://api.car.gnets.cn/app/h5/service_terms.html"

/*
 
 // 11执行远程锁车命令
 #define LockCar @"lock/lockBike.do"
 // 12执行远程解锁命令
 #define UnLockCar @"lock/unLockBike.do"
 // 13获取用户保险信息
 #define InsuranceInfo @"insur/getInsurInfo.do"

 
 */


