//
//  RegisterViewController.m
//  GNETS
//
//  Created by tcnj on 16/2/19.
//  Copyright © 2016年 CQZ. All rights reserved.
//

#import "RegisterViewController.h"
#import "RegisterTableViewCell.h"
#import "MyPickView.h"


static NSString *const CELLIDENTIFER = @"RegisterIdentifer";
@interface RegisterViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UITapGestureRecognizer * _tapGesture;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *titleArrays;//标题
@property (strong, nonatomic) NSMutableArray *texeArrays0;
@property (strong, nonatomic) NSMutableArray *texeArrays1;
@property (strong, nonatomic) NSMutableArray *texeArrays2;

@end

@implementation RegisterViewController
- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];//移除观察者
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _imeiStr = @""; _carid = @"";_telNum = @"";_userName = @"";_phone = @"";
    _province = @"";_city = @"";_area = @"";_address = @"";_dealerId = @"";_salesman = @"";
    _sex = @"0";
    [self initView];
}
- (void)initView
{
    [self setupNaviBarWithTitle:@"用户注册"];
    [self setupNaviBarWithBtn:NaviLeftBtn title:@"登录" img:@"back_icon"];
    [self setupNaviBarWithBtn:NaviRightBtn title:@"提交" img:nil];

    
    NSArray *oneArrays = [NSArray arrayWithObjects:@"IMEI",@"设备编号",@"数据卡号", nil];
    _texeArrays0 = [NSMutableArray arrayWithObjects:_imeiStr,_carid,_telNum, nil];
    NSArray *twoArrays = [NSArray arrayWithObjects:@"姓名",@"性别",@"联系电话",@"所在地区",@"详细地址", nil];
    _texeArrays1 = [NSMutableArray arrayWithObjects:_userName,@"",_phone,@"必选",_address ,nil];
    NSArray *thirdArrays = [NSArray arrayWithObjects:@"车辆品牌",@"车辆型号", nil];
    _texeArrays2 = [NSMutableArray arrayWithObjects:_dealerId,_salesman, nil];
    self.titleArrays = [NSMutableArray arrayWithObjects:oneArrays,twoArrays,thirdArrays, nil];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kMainScreenWidth, kMainScreenHeight-64) style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[RegisterTableViewCell class] forCellReuseIdentifier:CELLIDENTIFER];
    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyBoard)];
    _tapGesture.cancelsTouchesInView = NO;//关键代码

}
#pragma mark -手势
- (void)dismissKeyBoard{
    [self.view endEditing:YES];
}
#pragma mark -UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.titleArrays count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.titleArrays[section] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisterTableViewCell *cell = (RegisterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CELLIDENTIFER];

    cell.sectionIndex = indexPath.section;
    cell.rowIndex = indexPath.row;
    if (self.titleArrays.count>0)
    {
        cell.titleLab.text = self.titleArrays[indexPath.section][indexPath.row];
        if ([cell.titleLab.text isEqualToString:@"数据卡号"])
        {
            cell.lineView.hidden = YES;
        }else if ([cell.titleLab.text isEqualToString:@"详细地址"]){
            cell.lineView.hidden = YES;
        }else if ([cell.titleLab.text isEqualToString:@"业务员姓名"]){
            cell.lineView.hidden = YES;
        }else{
            cell.lineView.hidden = NO;
        }
    }
    NSUInteger section = indexPath.section;
    NSUInteger row = indexPath.row;
    DLog(@"输出section：%ld   row:%ld",section,row);
    switch (section) {
        case 0:
            DLog(@"输出值－－－－%@",_texeArrays0[row]);

            cell.textField.tag = 2000+row;
            cell.textField.text = _texeArrays0[row];
            cell.deviceLabel.text = _texeArrays0[row];
            break;
        case 1:
            cell.textField.tag = 3000+row;
            cell.textField.text = _texeArrays1[row];
            cell.deviceLabel.text = _texeArrays1[row];
            break;
        case 2:
            cell.textField.tag = 4000+row;
            cell.textField.text = _texeArrays2[row];
            break;
            
        default:
            break;
    }
    if (section == 1 && row == 3) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    if ((section=1&&row!=1) || (section=1&&row !=3) || (section==0&&row!=1))
    {
        cell.textField.delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldChanged:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:cell.textField];
    }
    cell.isSex = _isMan;
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyBoard];
    
    if (indexPath.section == 1 && indexPath.row == 1) {
        _isMan = !_isMan;
        if (_isMan == YES) {
            _sex = @"1";
        }else{
            _sex = @"0";
        }
        DLog(@"--------%@",_isMan?@"YES":@"NO");
        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:indexPath.row inSection:1],nil] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    if (indexPath.section == 1 && indexPath.row == 3) {
        
        MyPickView *pickView = [[MyPickView alloc] initWithFrame:CGRectMake(0, 0, kMainScreenWidth, kMainScreenHeight)];
        WEAKSELF;
        pickView.pickBlock = ^(NSString *proviceStr,NSString *cityStr,NSString *districtlistStr){
            
            DLog(@"%@-%@-%@",proviceStr,cityStr,districtlistStr);
            weakSelf.province = proviceStr; weakSelf.city = cityStr; weakSelf.area = districtlistStr;
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:1];
//            RegisterTableViewCell *cell = (RegisterTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
            NSString *tempStr = [NSString stringWithFormat:@"%@-%@-%@",proviceStr,cityStr,districtlistStr];
            [_texeArrays1 replaceObjectAtIndex:3 withObject:tempStr];
            [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:3 inSection:1],nil] withRowAnimation:UITableViewRowAnimationNone];
            
        };
        [self.view addSubview:pickView];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.f;
}
#pragma mark -UITextFieldDelegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    DLog(@"值：－－%ld",textField.tag);
    if (textField.tag == 2000) {
        [self getIMEIEvent:textField];
    }
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSUInteger index = textField.tag;
    
    if (index == 2000 || index ==2002 || index == 3002 || index == 3003)
    {
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    return YES;
}

- (void)textFieldChanged:(NSNotification*)noti{
    
    UITextField *textField = (UITextField *)noti.object;
    
    BOOL flag=[NSString isContainsTwoEmoji:textField.text];
    if (flag){
        SHOW_ALERT(@"不能输入表情!");
        textField.text = [NSString disable_emoji:textField.text];
    }
//    if (textField.tag == 2000) {
//        if (textField.text.length>8) {
//            textField.text = [textField.text substringToIndex:8];
//            DLog(@"第几位－－－－%@",textField.text);
//        }
//    }
    if (textField.tag == 2000) {
        _imeiStr = textField.text;
        [_texeArrays0 replaceObjectAtIndex:0 withObject:_imeiStr];
    }
    if(textField.tag == 2002){
        _telNum = textField.text;
        [_texeArrays0 replaceObjectAtIndex:2 withObject:_telNum];
    }
    
    if (textField.tag == 3000) {
        _userName = textField.text;
        [_texeArrays1 replaceObjectAtIndex:0 withObject:_userName];
    }else if(textField.tag == 3002){
        _phone = textField.text;
        [_texeArrays1 replaceObjectAtIndex:2 withObject:_phone];
    }else if(textField.tag == 3004){
        _address = textField.text;
        [_texeArrays1 replaceObjectAtIndex:4 withObject:_address];
    }
    
    if (textField.tag == 4000) {
        _dealerId = textField.text;
        [_texeArrays2 replaceObjectAtIndex:0 withObject:_dealerId];
    }else if(textField.tag == 4001){
        _salesman = textField.text;
        [_texeArrays2 replaceObjectAtIndex:1 withObject:_salesman];
        
    }

}

#pragma mark -UIButtonEvent
- (void)rightBtnAction
{
    DLog(@"提交");
    if (_imeiStr.length<=0 && _carid.length<=0 && _telNum.length<=0 && _userName.length<=0 && _phone.length<=0 && _province.length<=0 && _address.length<=0) {
        SHOW_ALERT(@"请您检查您的资料");
        return;
    }
    
    //加个旋转框
    [SVProgressHUD showWithStatus:@"正在提交..." maskType:SVProgressHUDMaskTypeClear];

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObjectWithNullValidate:_carid forKey:@"carId"];
    [dict setObjectWithNullValidate:_telNum forKey:@"telNum"];
    [dict setObjectWithNullValidate:_userName forKey:@"userName"];
//    [dict setObjectWithNullValidate:_idNum forKey:@"idNum"];
    [dict setObjectWithNullValidate:_phone forKey:@"phone"];
    [dict setObjectWithNullValidate:_province forKey:@"province"];
    [dict setObjectWithNullValidate:_city forKey:@"city"];
    [dict setObjectWithNullValidate:_area forKey:@"area"];
    [dict setObjectWithNullValidate:_address forKey:@"address"];
    [dict setObjectWithNullValidate:_dealerId forKey:@"carBrand"];
    [dict setObjectWithNullValidate:_salesman forKey:@"carModel"];
    [dict setObjectWithNullValidate:_sex forKey:@"sex"];

    NSString *registerUrl = [NSString stringWithFormat:@"%@%@",kProjectBaseUrl,REGISTERURL];
    [GNETS_NetWorkManger PostJSONWithUrl:registerUrl parameters:dict isNeedHead:NO success:^(NSDictionary *jsonDic) {
        [SVProgressHUD dismiss];
        NSUInteger code = [[jsonDic objectForKey:@"code"] integerValue];
        if (code == 1) {
            [JKPromptView showWithImageName:nil message:@"注册成功"];
            NSString *msgS = [NSString stringWithFormat:@"恭喜您，注册成功！您的账号为:%@，初始密码为:666666",_carid];
            UIAlertView *alerlView = [[UIAlertView alloc] initWithTitle:@"提示" message:msgS delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            alerlView.tag = 7777;
            [alerlView show];
        }
        
    } fail:^{
        [SVProgressHUD dismiss];
    }];
    
}
#pragma mark - 请求imei
- (void)getIMEIEvent:(UITextField *)textField{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    RegisterTableViewCell *cell = (RegisterTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    if (textField.text.length>0 && cell.deviceLabel.text.length==0) {
        
        NSString *imeiUrl = [NSString stringWithFormat:@"%@%@?imei=%@",kProjectBaseUrl,IMEIURLSTRING,textField.text];
        [GNETS_NetWorkManger GetJSONWithUrl:imeiUrl isNeedHead:YES success:^(NSDictionary *jsonDic) {
            NSUInteger code = [[jsonDic objectForKey:@"code"] integerValue];
            if (code == 1) {
                NSDictionary *dataDic = jsonDic[@"data"];
                _imeiStr = [NSString stringWithFormat:@"%@",dataDic[@"imei"]];
                [_texeArrays0 replaceObjectAtIndex:0 withObject:_imeiStr];
                _carid =[NSString stringWithFormat:@"%@",dataDic[@"carId"]];
                [_texeArrays0 replaceObjectAtIndex:1 withObject:_carid];
                [_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
            }else{
                _imeiStr = @"";
                SHOW_ALERT(jsonDic[@"errmsg"])
//                [JKPromptView showWithImageName:nil message:jsonDic[@"errmsg"]];
                [_texeArrays0 replaceObjectAtIndex:0 withObject:_imeiStr];
                [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
            }
            
        } fail:^{
            
        }];
    }
}
#pragma mark -UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 7777 && buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:NO];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
